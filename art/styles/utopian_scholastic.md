---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Utopian Scholastic
## [Examples](https://www.are.na/evan-collins-1522646491/utopian-scholastic)
---

Utopian Scholastic as an aesthetic appeared alongside the creation of the internet, and educational software, but has its roots in the advertising of the mid to late 1980s, and even movements like still lives.

There is little research or scholarship done about the aesthetic at this point, but I would draw a tenuous link through advertisement collages like those of [Braldt Bralds](https://www.braldtbraldsstudio.com/Image.asp?ImageID=2567802&apid=1&gpid=1&ipid=1&AKey=FRBDN7S4). Which was a part of the movement now known under the name "frasurbane".

The qualities that make up the utopian scholastic aesthetic are pretty hotly debated, but there are a few key elements

- a optimism for both the world and technology
- colonialist undertones
- "end of history" motifs with "timeless" aesthetics
- knowledge for its own sake in a sort of reneissance brought about by the internet

The aesthetic likely draws its name from a facebook group entitled [utopian scholastic designs from a pre 9/11 world](https://www.facebook.com/groups/410769822590075), though I could find at least one reference to the name being used to describe an aesthetic in a piece of fiction titled [Timeline 15: Another End of History by Silas-Coldwine](https://www.deviantart.com/silas-coldwine/art/Timeline-15-Another-End-of-History-638301498).

The origins of the aesthetic are hard to trace because so much of the designs relied on the increasing access to the internet, and uncredited use of stock photos from texture CDs that were circulated at the time.

This often created a mish-mash of similar aesthetic designs from various artists; combining cultures, regions, and ideas into covers for educational software, textbooks, or commercials.

One of the most famous examples of this kind of style is [Walter Wick](https://wikiless.org/wiki/Walter_Wick?lang=en) known for his work on the I-SPY books. These books use [real photography](https://www.youtube.com/watch?v=sz8luEznxbM) rather than stock footage to create their signature eye-candy look.