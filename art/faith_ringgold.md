---
type: note
tags:
  - art
  - artist
  - identity
  - multimedia
  - quilting
aliases:
  - faith ringgold
creation-date: 2023-10-17
modification-date: 2023-12-27
---
# Faith Ringgold
Faith Ringgold is a New York born multimedia artist, inventor of the story quilt, and children's book author. Her work is deeply imbued with familial relationships, racial politics, and forward-thinking style.

Her family is central to a lot of her work, with her mom's work in fashion design informing a lot of her textile work and fabric choices, and her daughter's penmanship appearing on the story quilts.

I think what I find most interesting about ringgolds work is her juxtaposition of childhood innocence with an unjust world. Many similar appeals to saving the children fail to center the actual feelings and opinions of the people they imagine themselves guardians of. Ringgold uses childhood optimism as a galvanizing force to bring about a world where wanting things to be better dominates the polity. In Tar Beach #2 her fictional persona imagining flying over the Union building her father is not allowed to be a member of, and becoming the owner that building merely out of raw desire to. That lack of agency fought against with imagination provides a coherent political framework for our childhood dreams to mature into, and expounds on the injustices that must be fought before any progress into the magical may begin.