---
tags: [technology, computing, design]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# A Brief Rant on the Future of Interaction Design

## Bret Victor

### [source](http://worrydream.com/ABriefRantOnTheFutureOfInteractionDesign/)

---

The article pretty much offers what it says on the tin. It seeks to outline Victor's concern that we are chasing design in the wrong direction when it comes to personal electronic devices.

They start by defining a tool as a solution that interfaces the problem with a human, like how the head of a hammer solves the problem, while the handle allows it to be effectively used by a human.

Bret contends that the paradigm of "pictures under glass" sacrifices a lot of the interface abilities that the complex muscles and joints in our hands have. It offers relatively little feedback for our bodies to intuit and get information from, and in turn forces the interaction paradigm for technology to rely on two dimensions.

Though this is a brief rant that does not attempt to offer solutions, there is also a [follow up](http://worrydream.com/ABriefRantOnTheFutureOfInteractionDesign/responses.html) where Bret answers some frequent responses.
