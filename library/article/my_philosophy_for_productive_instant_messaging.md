---
tags: [communication, technology]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# My Philosophy for Productive Instant Messaging

## Drew DeVault

### [Source](https://drewdevault.com/2021/11/24/A-philosophy-for-instant-messaging.html)

---

*abstract*

Many people use instant message as more of a tool to catalogue information than a place to have an active discussion. This leads to a lot of wasted time and frustration

> The most important trait to consider when using IM software is that it is *ephemeral*, and must be treated as such.

#### Core Ideas

- You should not "catch up" on discussions
- Move important information to a different location
- Don't expect people to be "in the know" for a discussion they weren't present for
- try to give all of the information to a question at once, because instant messaging is asynchronous and should be treated as such
