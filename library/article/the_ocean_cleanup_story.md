---
tags: [sustainability]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---
# the_ocean_cleanup_story

This is a section from [[scooping_plastic_out_of_the_ocean_is_a_losing_game]]

---

The Ocean Cleanup is a project founded in 2013 by 18yo Boyan Slat, and has raised more than $35 million to clean up the five gyres. It wants to do this by using large booms that catch plastic as it flows through the ocean, which can then be transported to shore by a barge.

While the program has been able to raise awareness and improve the scientific understanding of where plastic is coming from and what kinds are in the sea, they have had 0 success removing any on a large scale. At time of writing (9/23/21) None of these booms have ever been deployed nor have all the risks been explored<sup>1 2</sup>.

An analysis<sup>3</sup> by Sönke Hohn has shown that even if 200 of these will be deployed successfully and ran nonstop until 2150, only 5% of the ocean's plastic would be recovered with an "optimistic" calculation.

One successful thing they have implemented are river booms that funnel garbage into an anchored collection barge. While it doesn't extend through the river, it is far better than nets in the middle of the ocean.

[1](http://www.southernfriedscience.com/i-asked-15-ocean-plastic-pollution-experts-about-the-ocean-cleanup-project-and-they-have-concerns/)

[2](https://www.theatlantic.com/science/archive/2019/01/ocean-cleanup-project-could-destroy-neuston/580693/)
