---
tags: [culture, spirituality, technology]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Magic Numbers

## Alana Mohamed

### [source](https://reallifemag.com/magic-numbers/)

---

This article talks about the rise of tarot readers, fortune tellers, and mediums that use tiktok, and purport the algorithm as a cosmic force that directs the message to those who need it. Alana attempts to put this into the larger context of buursts in spirituality over times of societal trauma, and explain this rise through the mythos that technology companies build.

The article starts by describing the rise of the tiktok algorithm in particular as some sort of omnipotent entity that knows users better then themselves. I have seen a lot of friends discuss this in the context of their own usage of the app, and its interesting to hear that people have been capitalizing on the black box nature to exploit peoples spirituality.

It goes on to put this in the greater context of bursts in spirituality that have come from times of great national struggle. The examples it cites are the rise in spirituality and alternate belief systems during the collapse of the Roman empire, and the growth of mediums and psychics after the civil war in the U.S

The focus shifts towards the underlying technology, how algorithms have positioned themselves as a magical system that cannot be understood, - worth noting that we are seeing this in the growth of AI and blockchain technologies - and hides the immense amount of human labor, moderation, and curation that is needed to prop these systems up.

The mythos building that tech giants use to market themselves requires a sort of mysticism, and the article points out times that technology has integrated itself into spirituality in the past. Specifically citing William Mumler's "photographs of the dead", or the attempts from a U.S senator in the mid 19th century to build a telegraph that could connect the physical and spiritual world.

These sorts of charlatans are propped up from behind by capitalist greed and marketing that preys on people in times of strife. The only difference now is the audience is expanded and the spirit is the algorithm.

> In each instance, a new technology is defined not by its material impact, but lofty ideals that resonate with an increasingly desperate population.