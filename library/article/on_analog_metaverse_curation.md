---
tags: [philosophy, organization, pedagogy]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# On Analog Metaverse Curation

## Pr0ph3t

### [source](https://theofuturism.substack.com/p/on-analog-metaverse-curation)

---

On Analog Metaverse curation seeks to reframe our understanding of the personal library. No longer is it simply a collection of books once read or a stack of books to read.

> Building a library, a physical collection of important books that you own, is a work of subcreation. Your library is an Analog Metaverse that allows you to dip into and out of a personalized set of preserved realities at your whim. The connections between the works your read, the ones you choose to own, and your own thoughts and experiences build a unique and precious viewpoint that you have to share.

### I. Physical Information Inhabiting Space

Pro0ph3t explains the need of a physical personal library extends beyond the need to collect books. The physical presence matters, and it informs a lifestyle and experience that -while still importantcannot be afforded by an audiobook or Ebook.

### II. The Responsibility of Curation

No two libraries can ever be the same, and you cannot start making a library with an end goal in mind. The books must be personally meaningful, and removed when that ceases to become the case.

> The library is a breathing organism, accreting and shedding ideas so that it always reflects and activates your mind.

You cannot let the systems you create to triage information boss you around. A "to read" pile ceases to serve it's function when the books in that list are no longer appealing to you. Your library is there to serve you, and it must reflect yourself and aid in creating a physical environment that allows you to use it to the fullest extent.

### III. Joyful Misuse of Ideas

Proph3t attempts to give weight to the understanding that knowledge doesn't exist in a vacuum. Our brains make connections, whether logical on the face or not. It is not up to us to impose the understandings of others onto our own knowledge.

In a personal library, you must arrange things in the ways that benefit you, rather than appeasing some notion of "sensibleness".

> We restrict our reading so that we don't drown out wisdom by sheer volume. We pursue varied and twisting paths through the library because we never know what might lie buried underneath the next page.
