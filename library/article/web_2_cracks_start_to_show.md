---
tags: [web, technology, culture, security]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Web 2.0 Cracks Start to Show

## Xeni Jardin

### [source](https://www.wired.com/2005/10/web-2-0-cracks-start-to-show/)

---

This is an article that I actually have a lot of problems with. It disucsses what it sees as the pitfalls of web 2.0, but feels like everyone is missing the point. It derides user generated content, describes simple scams and grifts, but misses the greatest failure of web 2.0

The centralization of services onto platforms and introduction of profit motive from the internet directly; not just as a redirect to the physical world, brought all of the dregs of capitalism from the real world onto the internet.

People act as if the anonymity of the internet is what propels all of these things forward, but it falls back on this calvinist idea of man as inherently evil. Emphasizing that when left to his own devices and the promise of no consequences, human beings would immediately start brutalizing each other.

This completely misses the entire point of what is actually motivating this, the world is not full of self-motivated psychopaths waiting to hurt people, it encourages it tacitly through greed and actively through fear of becoming destitute. That is what introduced the poison to Web 2.0, not the flood of new users.
