---
tags: [history, anthropology, economics]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Debt: The First 5000 Years

## David Graeber

---

## The Myth of Barter

from chapter 2 of [[debt_the_first_5000_years|Debt: The First 5000 Years]]

> For every subtle and complicated question there is a perfectly simple and straightforward answer… which is wrong
> — H.L Minken

The chapter begins by explaining that the only difference between a debt and an obligation is money, and that both of these came to the earth around the same time.

The earliest works of moral philosophy, and the earliest surviving examples of writing all center around the concept of money and debt.

Economists speak of the origins of debt as coming after barter and money, but this is untrue.

Graeber explains this as 2 factors

- coinage survives when debt records do not
- debt is a difficult subject for economists to discuss because it is impossible to pretend that lending and borrowing money are on purely economic motiviations

> What is the difference between a loan to a stranger and a loan to one's cousin.

Economists position money as a proceeder to barter and one that allows for much more efficient trade since it is a unified value of exchange which doesn't require a "double coincidence" of needs.

This is not presented as something that actually happened, but as an imaginary difficult economy. This is false because it starts from our society and removes all the money while replacing it with nothing.

This is explained by economists over and over again because it is seen in Adam Smith's speech at the University of Glasgow where the field of economics as a study was born. However, Adam Smith did not come up with this idea himself, rather he acquired it from aristotle's speculation on how societies before money "must have operated".

As columbus and other colonizers sought to reap gold and silver from the world, it was assumed that there would be no barter economies, since every community must have a government and all governments issue money.
