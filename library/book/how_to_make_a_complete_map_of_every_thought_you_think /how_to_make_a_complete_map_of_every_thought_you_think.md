---
tags: [organization, knowledge_management, productivity]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# How to Make a Complete Map of Every Thought You Think

## Lion Kimbro

---

This is a book that is pretty well explained by it's title, here are my notes on all that I read with the exception of the "materials section", which I skipped as that part isn't relevant to me.

[[general_principles]]

[[information_density]]

[[page_layout]]

[[partitioning]]
