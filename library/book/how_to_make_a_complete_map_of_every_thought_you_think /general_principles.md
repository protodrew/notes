---
tags: [organization,knowledge_management,productivity]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# General Principals

from [[how_to_make_a_complete_map_of_every_thought_you_think]]

The General principles section steps away from the granular materials and floaty goals, and focuses on the physical ways your information should be presented, collected, and organized.
