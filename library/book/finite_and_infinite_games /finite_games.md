---
tags: [philosophy, psychology, culture]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Finite Games

from [[finite_and_infinite_games]]

Finite Games are described as requiring everyone to agree when the game is over, what the bounds are beforehand, and set it within temporal or spatial barriars.
