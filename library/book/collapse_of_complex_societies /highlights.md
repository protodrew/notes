---
tags: [history, anthropology]
type: book
alias: Collapse of Complex Civilizations Highlights
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Highlights
## From [[the_dawn_of_everything]]
### Joseph A. Tainter
## 1 Introduction to Collapse
### Page 25 @ 15 March 2023 09:50:31 PM
*The implication is clear: civilizations are fragile, impermanent things. This fact inevitably captures our attention, and however we might wish otherwise, prompts disturbing questions.*
### Page 30 @ 16 March 2023 03:46:56 AM
*Every thoughtful person who ponders the bureaucratic and technological pressures on ordinary life today must wonder whether it is possible for a society to strangle on its own complexities…*
### Page 31 @ 16 March 2023 03:53:17 AM
*Explanations of collapse have tended to be ad hoc, pertaining only to one or a few societies, so that a general understanding remains elusive.*
## What is Collapse?
### Page 33 @ 16 March 2023 03:54:48 AM
*Collapse is manifest in such things as:
a lower degree of stratification and social differentiation;
less economic and occupational specialization, of individuals, groups, and territories;
less centralized control; that is, less regulation and integration of diverse economic and political groups by elites;
less behavioral control and regimentation;
less investment in the epiphenomena of complexity, those elements that define the concept of `civilization': monumental architecture, artistic and literary achievements, and the like;
less flow of information between individuals, between political and economic groups, and between a center and its periphery;
less sharing, trading, and redistribution of resources;
less overall coordination and organization of individuals and groups;
a smaller territory integrated within a single political unit.*
### Page 35 @ 16 March 2023 03:56:45 AM
*There is no point on such a scale at which complexity can be said to emerge. Hunting bands and tribal cultivators experience changes in complexity, either increases or decreases, just as surely as do large nations.*
## The Harappan Civilization 6
### Page 37 @ 16 March 2023 03:59:16 AM
*(the `Hundred Schools')*
---
interesting
### Page 38 @ 16 March 2023 04:08:41 AM
*At some centers, the Harappan occupation was followed by people who lived among the ruins in flimsy huts, seemingly after the complete breakdown of civil authority.*
## Mesopotamia 7
### Page 40 @ 16 March 2023 04:10:38 AM
*This attempt to maximize economic and political power led to a rapid collapse, with disastrous consequences for southern Mesopotamia. Over the next millennium or so there was a 40 percent reduction in the number of settlements, and a 77 percent reduction in settled area.*
## The Egyptian Old
### Page 42 @ 16 March 2023 07:54:56 PM
*The last ruler of the Sixth Dynasty, Phiops II, built a magnificent funerary monument even as the declining power of the royal family was felt sharply at the close of his reign.*
### Page 42 @ 16 March 2023 07:53:21 PM
*In the First Intermediate Period national centralization collapsed, and was replaced by a number of independent and semi-independent polities. There were many rulers and generally short reigns. Royal tombs became less elaborate.*
## Minoan Civilization 9
### Page 45 @ 16 March 2023 08:15:06 PM
*They were thereafter repeatedly destroyed by earthquakes, and up to the final collapse were each time rebuilt more splendidly than before.*
### Page 45 @ 16 March 2023 08:17:44 PM
*Palaces functioned as administrative centers, as warehouses, and as controlling nodes in the economy*
### Page 45 @ 16 March 2023 08:19:10 PM
*There was administrative writing: records included the contents of armories, and indicate that goods were directed to the palace, and from there redistributed.*
### Page 45 @ 16 March 2023 08:48:56 PM
*For most of Minoan history Crete seems to have been peaceful, for the palaces were unfortified and the scenes on the frescoes peaceful. About 1500 B.C., however, a powerful earthquake caused widespread destruction, and thereafter there were major changes. An earlier script, undeciphered but known as Linear A, was replaced by the Greek Linear B. New methods of warfare were introduced, involving new kinds of arms and the horse.*
---
natural disaster puts civ into state of duress
## Mycenaean Civilization 10
### Page 47 @ 16 March 2023 08:54:08 PM
*The Linear B tablets from Pylos indicate that this kingdom was divided into 16 administrative districts, each controlled by a governor and deputy*
## The Lowland Classic Maya 12
### Page 50 @ 17 March 2023 01:07:46 AM
*At a cost of great effort, basalt monuments were deliberately and systematically mutilated and destroyed, and subsequently buried*
### Page 52 @ 16 March 2023 09:04:07 PM
*some remnant populations tried to carry on city life*
### Page 53 @ 16 March 2023 09:06:02 PM
*In the later phase of Teotihuacan's dominance military themes became prominent in art. The flow of some goods into the city was reduced. About 700 A.D. Teotihuacan abruptly collapsed. The politically and ceremonially symbolic center of the city, the Street of the Dead and its monuments, was systematically, ritually burned*
### Page 54 @ 16 March 2023 09:06:41 PM
*This remnant population sealed off doorways, and partitioned large rooms into smaller ones. A period of political fragmentation followed.*
### Page 54 @ 17 March 2023 01:11:00 AM
*Within a few generations population at Monte Alban had declined to about 18 percent of its peak level, and more defensive walls were built*
### Page 57 @ 17 March 2023 01:18:49 AM
*The dead were buried in city water canals and plaza drains. As walls crumbled, ramps were built to reach the still usable upper rooms*
### Page 69 @ 19 March 2023 04:39:31 PM
*What populations remain in urban or other political centers reuse existing architecture in a characteristic manner. There is little new construction, and that which is attempted concentrates on adapting existing buildings.*
### Page 70 @ 27 April 2023 05:35:46 PM
*When a building begins to collapse, the residents simply move to another*
## Complexity
### Page 70 @ 05 May 2023 03:33:01 PM
*Complex societies tend to be what Simon has called `nearly decomposable systems'*
### Page 70 @ 05 May 2023 03:33:26 PM
*they are at least partly built up of social units that are themselves potentially stable and independent, and indeed at one time may have been so.*
### Page 70 @ 05 May 2023 03:33:38 PM
*To the extent that these states, ethnic groups, or villages retain the potential for independence and stability, the collapse process may result in reversion (decomposition) to these `building blocks' of complexity*
## Simpler Societies 24
### Page 71 @ 05 May 2023 03:33:51 PM
*The citizens of modern complex societies usually do not realize that we are an anomaly of history.*
### Page 71 @ 05 May 2023 03:34:55 PM
*The small, acephalous communities that have dominated our history were not homogeneous. The degree of variation among such societies is substantial.*
### Page 71 @ 05 May 2023 03:36:05 PM
*societies tend to be organized on the basis of kinship, with status familial and centered on the individual*
---
over generalized. see dawn of everything notes
### Page 72 @ 05 May 2023 03:37:04 PM
*Leadership in the simplest societies tends to be minimal. It is personal and charismatic, and exists only for special purposes. Hierarchical control is not institutionalized, but is limited to definite spheres of activity at specific times, and rests substantially on persuasion*
### Page 72 @ 05 May 2023 03:37:18 PM
*Equality in these societies lies in direct, individual access to the resources that sustain life, in mobility and the option to simply withdraw from an untenable social situation,*
### Page 72 @ 05 May 2023 04:07:52 PM
*Personal political ambition is either restrained from expression, or channeled to fulfill a public good. The route to an elevated social position is to acquire a surplus of subsistence resources, and to distribute these in such a way that one establishes prestige in the community*
### Page 73 @ 05 May 2023 04:08:46 PM
*Since his influence is limited to his faction, extending that influence means extending the size of the following. At the same time, the loyalty of his existing followers must be constantly renewed through generosity*
### Page 73 @ 05 May 2023 04:08:58 PM
*As a Big Man attempts to expand his sphere of influence, he is likely to lose the springboard that makes this possible*
### Page 73 @ 05 May 2023 04:53:54 PM
*Other simple societies are organized at higher levels of political differentiation. There are true, permanent positions of rank in which authority resides in an office, rather than an individual,*
### Page 73 @ 05 May 2023 04:54:00 PM
*Inequality pervades such societies, which tend to be larger and more densely populated to a degree coordinate with their increased complexity.*
### Page 73 @ 05 May 2023 04:54:22 PM
*political organization extends beyond the community level. Accordingly, economic, political, and ceremonial life transcend purely local concerns. In the classic chiefdoms of Polynesia, entire islands would often be integrated into a single polity.*
### Page 73 @ 05 May 2023 04:54:48 PM
*As complexity and number of members grow, individuals must increasingly be socially categorized, so that appropriate behavior between persons is prescribed more by the impersonal structure of society and less by kin relations.*
### Page 74 @ 05 May 2023 04:55:31 PM
*The authority to command in such chiefdoms is not unrestrained.*
### Page 74 @ 05 May 2023 04:55:15 PM
*Claims of followers obligate a chief to respond positively to requests. Chiefly generosity is the basis of politics and economics: downward distribution of amassed resources ensures loyalty.*
## States 26
### Page 74 @ 05 May 2023 04:55:52 PM
*Too much allocation of resources to the chiefly apparatus, and too little return to the local level, engender resistance.*
### Page 74 @ 05 May 2023 05:48:34 PM
*Anthropologists have had some difficulty defining the concept `state.' It is something that seems clearly different from the simplest, acephalous human societies, but specifying or enumerating this difference has proven an elusive goal.*
### Page 74 @ 05 May 2023 05:56:29 PM
*Whether it is more profitable to view sociopolitical evolution as traversing a continuum of complexity, or as characterized by discrete stages or levels, is a matter pertinent to understanding collapse*
### Page 76 @ 05 May 2023 06:00:07 PM
*States are, to begin with, territorially organized. That is to say, membership is at least partly determined by birth or residence in a territory, rather than by real or fictive kin relations.*
### Page 76 @ 05 May 2023 07:10:28 PM
*government is legitimately constituted, which is to say that a common, society-wide ideology exists that serves in part to validate the political organization of society*
### Page 76 @ 05 May 2023 07:09:37 PM
*States tend to be overwhelmingly concerned with maintaining their territorial integrity. This is, indeed, one of their primary characteristics. States are the only kind of human society that does not ordinarily undergo short-term cycles of formation and dissolution*
### Page 77 @ 05 May 2023 07:11:17 PM
*Despite an institutionalized authority structure, an ideological basis, and a monopoly of force, the rulers of states share at least one thing with chiefs and Big Men: the need to establish and constantly reinforce legitimacy*
### Page 77 @ 05 May 2023 07:33:15 PM
*Hierarchy and complexity, as noted, are rare in human history, and where present require constant reinforcement. No societal leader is ever far from the need to validate position and policy, and no hierarchical society can be organized without explicit provision for this need.*
### Page 77 @ 05 May 2023 07:41:26 PM
*Decline in support will not necessarily lead to the fall of a regime, for to a certain extent coercion can replace commitment to ensure compliance. Coercion, though, is a costly, ineffective strategy which can never be completely or permanently successful.*