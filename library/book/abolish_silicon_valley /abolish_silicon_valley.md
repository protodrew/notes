---
tags: [technology, economics]
type: book
creation-date: 2023-10-19
modification-date: 2023-10-19
---

# Abolish Silicon Valley

## *How To Liberate Technology from Capitalism*

## Wendy Liu

---

A memoir by Wendy Liu about her experience in the silicon valley side of the tech bubble, and her failed startup, followed by general critiques of capitalism both in the tech industry and the world at large

> "No matter how good our automation technology, there was no viable business model in which we would actually achieve our goal of giving workers back their time"`

> "When everything's a commodity, even human suffering can be an economic boom"

> "Here's another way to understand profit: as an indicator of inefficiency, a sign that something has gone wrong in the factors that shape a given market. Some level of profit may be acceptable in the early stages or when necessary for expansion, but if a corporation has swelled to billion-dollar heights and is still turning to new frontiers to maintain profit margins, that's a problem"

### Core Ideas

- Startup culture is unsustainable
- The lack of ethics in tech education leads to worse products and a worse world
- A healthy work-life balance is seen as a weakness by startup people
- Humanities provides an important greater context to the work done by other sectors, and it's lack of perceived value in the current economic climate is a problem
- The core flaws of the tech industry are symptomatic of the larger failed system of capitalism
- Profit should be redirected to workers or greater society, rather than being the goal of a corporation

### Final Thoughts

equal parts memoir, analysis, and proposal. This book should be an essential read for anyone in or around the tech industry, and even people who aren't. I loved this book and it really opened my eyes to a lot of potentials I haven't considered.

[[proposed_solutions_in_abolish_silicon_valley]]
