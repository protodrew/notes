---
tags: [art,marxism,theory,economics]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# The Dialectics of Art
## John Molyneux
---

Dialectics of art attempts to look at art through a marxist lens to define and understand how art functions and clashes with capitalist hegemony.

Chapter one sees Molyneux articulating the relationship of artists to Marx's theory of alienated labor. In it he seeks to explain that the act of producing art does not make one an artists; pointing towards Marx's story of John Milton as an accomplished author who is able to produce "as a silkworm produces silk" and set the rate of his own labor, opposed to the writer who works on compendiums under the will of a publisher. Rather than claiming art does not involve alienated labor, he posits that the raw unalienated act of creation is what differentiates art from media.

Chapter two focuses on the criticism of art, attempting to develop a framework of critique not as a critical panacea, but as a reference point towards the larger discussion of how criticism and art exist within and outside of their eras. Criticism of one piece can change over generations as one or more crucial elements become more or less prominent within the larger artistic conversation.