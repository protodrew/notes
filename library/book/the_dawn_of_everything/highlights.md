---
tags: [culture, history, anthropology]
type: book
alias: Dawn of Everything Highlights
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Highlights
## From [[the_dawn_of_everything]]
## 3 Unfreezing the Ice Age: In and out of Chains: the Protean Possibilities of Human Politics
### Page 118 @ 01 April 2023
*In some ways, accounts of 'human origins' play a similar role for us today as myth did for ancient Greeks or Polynesians, or the Dreamtime for indigenous Australians*
### Page 119 @ 01 April 2023
*'the bottom dropped out of human history.'*
### Page 129 @ 01 April 2023
*Boehm's own work is revealing in this regard. An evolutionary anthropologist and a specialist in primate studies, he argues that while humans do have an instinctual tendency to engage in dominance-submissive behaviour, no doubt inherited from our simian ancestors, what makes societies distinctively human is our ability to make the conscious decision not to act that way.*
## 4 Free People, the Origin of Cultures, and the Advent of Private Property: (Not Necessarily in that order)
### Page 175 @ 04 April 2023
*For the most part, all we're left with as an alternative to our mundane lives are our 'national holidays': frantic periods of over-consumption, crammed in the gaps between work, in which we entertain solemn injunctions that consumption isn't really what matters about life*
### Page 140 @ 06 April 2023
*When forager bands gather into larger residential groups these are not, in any sense, made up of a tight-knit unit of closely related kin; in fact, primarily biological relations constitute on average a mere 10 per cent of total membership.*
### Page 141 @ 06 April 2023
*Society', insofar as we can comprehend it at that time, spanned continents*
### Page 142 @ 06 April 2023
*most peoples' social worlds growing more parochial, their lives and passions more likely to be circumscribed by boundaries of culture, class and language*
### Page 143 @ 06 April 2023
*it's difficult to exercise arbitrary power in, say, January over someone you will be facing on equal terms again come July. The hardening and multiplication of cultural boundaries can only have reduced such possibilities*
### Page 143 @ 06 April 2023
*So, as a first approximation, we can speak of an egalitarian society if (1) most people in a given society feel they really ought to be the same in some specific way, or ways, that are agreed to be particularly important; and (2) that ideal can be said to be largely achieved in practice*
### Page 144 @ 06 April 2023
*egalitarian societies' are those where everyone (or almost everyone) agrees that the paramount values should be, and generally speaking are, distributed equally*
### Page 145 @ 06 April 2023
*say that cereal-farming was responsible for the rise of such states is a little like saying that the development of calculus in medieval Persia is responsible for the invention of the atom bomb*
### Page 145 @ 06 April 2023
*Roughly 6,000 years stand between the appearance of the first farmers in the Middle East and the rise of what we are used to calling the first states; and in many parts of the world, farming never led to the emergence of anything remotely like those states.*
### Page 146 @ 06 April 2023
*We are creatures of excess, and this is what makes us simultaneously the most creative, and most destructive, of all species*
### Page 146 @ 06 April 2023
*the dominant view among anthropologists nowadays is that the only way to maintain a truly egalitarian society is to eliminate the possibility of accumulating any sort of surplus at all.*
### Page 147 @ 06 April 2023
*Truly egalitarian societies, for Woodburn, are those with 'immediate return' economies: food brought home is eaten the same day or the next; anything extra is shared out, but never preserved or stored.*
### Page 148 @ 15 April 2023
*American citizens have the right to travel wherever they like – provided, of course, they have the money for transport and accommodation. They are free from ever having to obey the arbitrary orders of superiors – unless, of course, they have to get a job.*
### Page 150 @ 15 April 2023
*The freedom to abandon one's community, knowing one will be welcomed in faraway lands; the freedom to shift back and forth between social structures, depending on the time of year; the freedom to disobey authorities without consequence – all appear to have been simply assumed among our distant ancestors*
### Page 151 @ 15 April 2023
*16*
### Page 151 @ 15 April 2023
*They referred to the apex of development as 'commercial society', in which a complex division of labour demanded the sacrifice of primitive liberties but guaranteed dazzling increases in overall wealth and prosperity.*
### Page 152 @ 15 April 2023
*John Stuart Mill protested that 'All the labour-saving machinery that has hitherto been invented has not lessened the toil of a single human being*
### Page 154 @ 15 April 2023
*The fact that many hunter-gatherers, and even horticulturalists, only seem to have spent somewhere between two and four hours a day doing anything that could be construed as 'work' was itself proof of how easy their needs were to satisfy.*
### Page 156 @ 15 April 2023
*what some prehistorians had assumed to be technical ignorance was really a self-conscious social decision: such foragers had 'rejected the Neolithic Revolution in order to keep their leisure'.*
### Page 156 @ 15 April 2023
*If there is a fundamental difference here from the biblical story, it's that the Fall (according to Sahlins) didn't happen just once. We didn't collapse and then begin slowly to pull ourselves back up. When it comes to labour and affluence, every new technological breakthrough seems to cause us to fall yet further*
### Page 159 @ 15 April 2023
*archaeologists often point out, Poverty Point is 'a Stone Age site in an area where there is no stone', so the staggering quantities of lithic tools, weapons, vessels and lapidary ornaments found there must all have been originally carried from somewhere else*
### Page 160 @ 15 April 2023
*Most experts today view its monuments as expressions of sacred geometry, linked to calendar counts and the movement of celestial bodies. If anything was being stockpiled at Poverty Point, it may well have been knowledge: the intellectual property of rituals, vision quests, songs, dances and images.*
### Page 161 @ 15 April 2023
*The various configurations of their mounds and ridges adhere to strikingly uniform geometrical principles, based on standard units of measurement and proportion apparently shared by early peoples throughout a significant portion of the Americas. The underlying system of calculus appears to have been based on the transformational properties of equilateral triangles, figured out with the aid of cords and strings, and then extended to the laying-out of massive earthworks.*
### Page 161 @ 15 April 2023
*it means that someone had to convey knowledge of geometric and mathematical techniques for making accurate spatial measurements, and related forms of labour organization, over very long distances. If this were the case, it seems likely that they also shared other forms of knowledge as well: cosmology, geology, philosophy, medicine, ethics, fauna, flora, ideas about property, social structure, and aesthetics*
### Page 162 @ 15 April 2023
*The obvious question at this juncture must surely be: why isn't Poverty Point better known to audiences the world over? Why doesn't it feature more prominently (or at all) in discussions on the origins of urban life, centralization and their consequences for human history*
### Page 166 @ 16 April 2023
*As we've seen, indigenous critics like Kandiaronk, caught in the rhetorical moment, would frequently overstate their case, even playing along with the idea that they were blissful, innocent children of nature. They did this in order to expose what they considered the bizarre perversions of the European lifestyle. The irony is that, in doing so, they often played into the hands of those who argued that – being blissful and innocent children of nature – they also had no natural rights to their land*
### Page 167 @ 16 April 2023
*In parts of Australia, these indigenous techniques of land management were such that, according to one recent study, we should stop speaking of 'foraging' altogether, and refer instead to a different sort of farming.*
### Page 171 @ 16 April 2023
*Those who today describe people like the Calusa as 'atypical' because they had such a prosperous resource base want us to believe, instead, that ancient foragers chose to avoid locations of this kind, shunning the rivers and coasts (which also offered natural arteries for movement and communication), because they were so keen to oblige later researchers by resembling twentieth-century hunter-gatherers (the sort for which detailed scientific data is available today). We are asked to believe that it was only after they ran out of deserts and mountains and rainforests that they reluctantly started to colonize richer and more comfortable environments. We might call this the 'all the bad spots are taken!' argument*
### Page 172 @ 16 April 2023
*Such was the fate of the Calusa and their ancient fishing and hunting grounds. When Florida was ceded to the British in the mid eighteenth century, the last handful of surviving subjects from the kingdom of Calos were shipped off to the Caribbean by their Spanish masters.*
### Page 172 @ 16 April 2023
*More recent evidence suggests a very different picture (or, as one Navajo informant put it when faced with an archaeological map of the terrestrial route via Beringia: 'maybe some other guys came over like that, but us Navajos came a different way').*
### Page 174 @ 16 April 2023
*In other words, while the court of the Natchez Sun was not pure empty theatre – those executed by the Great Sun were most definitely dead – neither was it the court of Suleiman the Magnificent or Aurangzeb. It seems to have been something almost precisely in between.*
### Page 174 @ 16 April 2023
*Even were it possible to write off Pleistocene mammoth hunters as some kind of strange anomaly, the same clearly cannot be said for the period that immediately followed the glaciers' retreat, when dozens of new societies began to form along resource-rich coasts, estuaries and river valleys, gathering in large and often permanent settlements, creating entirely new industries, building monuments according to mathematical principles, developing regional cuisines, and so on.*
### Page 175 @ 16 April 2023
*To construct the earthworks at Poverty Point, for instance, must have taken enormous amounts of human labour and a strict regime of carefully planned-out work, but we still have little idea how that labour was organized. Japanese archaeologists, surveying thousands of years' worth of Jōmon sites, have discovered all sorts of treasures, but they are yet to find indisputable evidence that those treasures were monopolized by any sort of aristocracy or ruling elite*
### Page 175 @ 16 April 2023
*In Hadza religion and the religions of many Pygmy groups, initiation into male (and sometimes female) cults forms the basis of exclusive claims to ownership, usually of ritual privileges, that stand in absolute contrast to the minimization of exclusive property rights in everyday, secular life. These various forms of ritual and intellectual property, Woodburn observed, are generally protected by secrecy, by deception and often by the threat of violence.*
### Page 176 @ 16 April 2023
*As British legal theorists like to put it, individual property rights are held, notionally at least, 'against the whole world'. If you own a car, you have the right to prevent anyone in the entire world from entering or using it*
### Page 177 @ 16 April 2023
*Almost anything else you can do with a car is strictly regulated: where and how you can drive it, park it, and so forth. But you can keep absolutely anyone else in the world from getting inside it*
### Page 178 @ 16 April 2023
*It was often the case that weapons, tools and even territories used to hunt game were freely shared – but the esoteric powers to safeguard the reproduction of game from one season to the next, or ensure luck in the chase, were individually owned and jealously guarded*
### Page 178 @ 16 April 2023
*Such forms of sacred property are endlessly complex and variable. Among Plains societies of North America, for instance, sacred bundles (which normally included not only physical objects but accompanying dances, rituals and songs) were often the only objects in that society to be treated as private property: not just owned exclusively by individuals, but also inherited, bought and sold*
### Page 178 @ 16 April 2023
*In what anthropologists refer to as totemic systems, of the kind we discussed for Australia and North America, the responsibility of care takes on a particularly extreme form. Each human clan is said to 'own' a certain species of animal – thus making them the 'Bear clan', 'Elk clan', 'Eagle clan' and so forth – but what this means is precisely that members of that clan cannot hunt, kill, harm or otherwise consume animals of that species. In fact, they are expected to take part in rituals that promote its existence and make it flourish*
### Page 179 @ 16 April 2023
*The defining feature of true legal property, then, is that one has the option of not taking care of it, or even destroying it at will*
### Page 181 @ 16 April 2023
*. If private property has an 'origin', it is as old as the idea of the sacred, which is likely as old as humanity itself. The pertinent question to ask is not so much when this happened, as how it eventually came to order so many other aspects of human affairs*
## 5 Many Seasons Ago: Why Canadian Foragers Kept Slaves and Their Californian Neighbours Didn't; Or, the Problem with 'modes of Production'
### Page 183 @ 16 April 2023
*When considering the broad sweep of history, most scholars either completely ignore this pre-agricultural world or write it off as some kind of strange anomaly: a false start to civilization*
### Page 184 @ 16 April 2023
*While the free peoples of North America's eastern seaboard nearly all adopted at least some food crops, those of the West Coast uniformly rejected them. Indigenous peoples of California were not pre-agricultural. If anything, they were anti-agricultural*
### Page 185 @ 16 April 2023
*The systematic rejection of all domesticated foodstuffs is even more striking when one realizes that many Californians and Northwest Coast peoples did plant and grow tobacco, as well as other plants – such as springbank clover and Pacific silverweed – which*
### Page 186 @ 16 April 2023
*It is curious how little anthropologists speculate about why this whole process of subdivision ever happened. It's usually treated as self-evident, an inescapable fact of human existence. If any explanation is offered, it's assumed to be an effect of language*
### Page 186 @ 16 April 2023
*Nostratic was believed to have existed sometime during the later Palaeolithic, or even to have been the original phylum from which every human language sprang.*
### Page 186 @ 16 April 2023
*It might seem strange to imagine linguistic drift causing a single idiom to evolve into languages as different as English, Chinese and Apache; but, given the extraordinarily long periods of time being considered here, even an accretion of tiny generational changes can, it seems, eventually transform the vocabulary and sound-structure, even the grammar of a language completely.*
### Page 187 @ 16 April 2023
*what we are seeing here also reflects a deeper continuity of culture-historical development, a process that tended to occur at various points in human history, when modern nation states were not around to order populations into neat ethno-linguistic groups. Arguably, the very idea that the world is divided into such homogeneous units, each with its own history, is largely a product of the modern nation state, and the desire of each to claim for itself a deep territorial lineage*
### Page 191 @ 16 April 2023
*As their refusal of agriculture implies, these processes were likely far more self-conscious than scholars usually imagine. In some cases, as we'll see, they appear to have involved explicit reflection and argument about the nature of freedom itself*
### Page 194 @ 16 April 2023
*Mauss thought the idea of cultural 'diffusion' was mostly nonsense; not for the reasons most anthropologists do now (that it's pointless and uninteresting),11 but because he felt it was based on a false assumption: that the movement of people, technologies and ideas was somehow unusual*
### Page 196 @ 16 April 2023
*Framed in this way, the question of how 'culture areas' formed is necessarily a political one. It raises the possibility that decisions such as whether or not to adopt agriculture weren't just calculations of caloric advantage or matters of random cultural taste, but also reflected questions about values, about what humans really are (and consider themselves to be), and how they should properly relate to one another. Just the kinds of issues, in fact, which our own post-Enlightenment intellectual tradition tends to express through terms like freedom, responsibility, authority, equality, solidarity and justice.*
### Page 197 @ 16 April 2023
*The region's 'Mediterranean' climate and tightly compressed topography of mountains, deserts, foothills, river valleys and coastlines made for a rich assortment of local flora and fauna, exchanged at inter-tribal trade fairs.*
### Page 197 @ 16 April 2023
*From the Klamath River northwards, there existed societies dominated by warrior aristocracies engaged in frequent inter-group raiding and in which, traditionally, a significant portion of the population had consisted of chattel slaves. This apparently had been true as long as anyone living there could remember. But none of this was the case further south. How exactly did this happen? How did a boundary emerge between one extended 'family' of foraging societies that habitually raided each other for slaves, and another that did not keep slaves at all?*
### Page 199 @ 16 April 2023
*Probably the most famous of these, wampum, did eventually come to be used as a trade currency in transactions between settlers and indigenous peoples of the Northeast, and was even accepted as currency in several American states for transactions between settlers (in Massachusetts and New York, for instance, wampum was legal tender in shops). In dealings between indigenous people, however, it was almost never used to buy or sell anything. Rather, it was employed to pay fines, and as a way of forming and remembering compacts and agreements.*
### Page 199 @ 19 April 2023
*Capitalism, on the other hand, involved constant reinvestment, turning one's wealth into an engine for creating ever more wealth, increasing production, expanding operations, and so forth.*
### Page 200 @ 19 April 2023
*The Yurok were what we've called 'possessive individualists'. They took it for granted that we are all born equal, and that it is up to each of us to make something of ourselves through self-discipline, self-denial and hard work. What's more, this ethos appears to have been largely applied in practice.*
### Page 203 @ 20 April 2023
*Nobles often compared themselves to mountains, with the gifts they bestowed rolling off them like boulders, to flatten and crush their rivals.*
### Page 203 @ 20 April 2023
*Commoners, including brilliant artists and craftspeople, were largely free to decide which noble house they wished to align themselves with; chiefs vied for their allegiance by sponsoring feasts, entertainment and vicarious participation in their heroic adventures.*
### Page 204 @ 20 April 2023
*In many ways, the behaviour of Northwest Coast aristocrats resembles that of Mafia dons, with their strict codes of honour and patronage relations; or what sociologists speak of as 'court societies*
### Page 204 @ 20 April 2023
*All this begins to make the anthropologists' habit of lumping Yurok notables and Kwakiutl artists together as 'affluent foragers' or 'complex hunter-gatherers' seem rather silly: the equivalent of saying a Texas oil executive and a medieval Egyptian poet were both 'complex agriculturalists' because they ate a lot of wheat*
### Page 204 @ 20 April 2023
*Do we start from the institutional structure (the rank system and importance of potlatch in the Northwest Coast, the role of money and private property in California), then try to understand how the prevailing ethos of each society emerges from it? Or did the ethos come first*
### Page 204 @ 20 April 2023
*To put the matter more technically, we might ask what ultimately determines the shape a society takes: economic factors, organizational imperatives or cultural meanings and ideas? Following in the footsteps of Mauss, we might also suggest a fourth possibility. Are societies in effect self-determining, building and reproducing themselves primarily with reference to each other*
### Page 207 @ 20 April 2023
*Cemeteries of Middle Pacific age, between 1850 BC and AD 200, reveal extreme disparities in treatments of the dead, something not seen in earlier times.*
### Page 209 @ 20 April 2023
*Seen one way, a slave-raider is stealing the years of caring labour another society invested to create a work-capable human being.36*
### Page 210 @ 20 April 2023
*Marxists, who refer to 'modes of production', do sometimes allow for a 'Tributary Mode,' but this has always been linked to the growth of agrarian states and empires, back to Book III of Marx's Capital.38 What really needs to be theorized here is not just the mode of production practised by victims of predation, but also that of the non-producers who prey on them*
### Page 211 @ 20 April 2023
*is that this kind of exploitation often took the form of ongoing relations between societies. Slavery almost always tends to do this, since imposing 'social death' on people whose biological relatives speak the same language as you and can easily travel to where you live will always create problems.*
### Page 212 @ 20 April 2023
*the main objective of slave-taking for the 'capturing society' seems to have been to increase its internal capacity for caring labour. What was ultimately being produced here, within Guaicurú society, were certain kinds of people: nobles, princesses, warriors, commoners, servants, and so on.*
### Page 217 @ 20 April 2023
*the technical language of behavioural ecology, fish are 'front-loaded'. You have to do most of the work of preparation right away*
### Page 218 @ 20 April 2023
*the capture of dried fish, or foodstuffs of any kind, was never a significant aim of Northwest Coast inter-group raiding. To put it bluntly, there's only so many smoked fish one can pile up in a war canoe*
### Page 219 @ 20 April 2023
*The simple reality is that there was no shortage of working hands in Northwest Coast households. But a good proportion of those hands belonged to aristocratic title holders who felt strongly that they should be exempted from menial work.*
### Page 219 @ 20 April 2023
*Indeed, the relation between title-holding nobles and their dependants seems to have been under constant negotiation. Sometimes it was not entirely clear who was serving whom:*
### Page 220 @ 20 April 2023
*Swadesh tells of a despotic [Nootka] chief who was murdered for 'robbing' his commoners by demanding all of his fishermen's catch, rather than the usual tributary portion. His successor outdid himself in generosity, saying when he caught a whale, 'You people cut it up and everyone take one chunk; just leave the little dorsal fin for me.'57*
### Page 220 @ 20 April 2023
*Title-holding aristocrats, locked in rivalry with one another, simply lacked the means to compel their own subjects to support their endless games of magnificence. They were forced to look abroad.*
### Page 221 @ 20 April 2023
*Yurok requirement for victors in battle to pay compensation for each life taken, at the same rate one would pay if one were guilty of murder. This seems a highly efficient way of making inter-group raiding both fiscally pointless and morally bankrupt*
### Page 254 @ 03 May 2023
*This was not exploitation of child labor, but an important religious act, freighted with significance. Special wood was brought from the mountain ridges; it was used for an important purification ritual. The gathering itself was a religious act, for it was a means of acquiring 'luck.' It had to be done with the proper psychological attitude of which restrained demeanor and constant thinking about the acquisition of riches were the chief elements.*
### Page 255 @ 03 May 2023
*To enhance his status and impress his ancestors, the nobleman of the Northwest Coast ladled candlefish oil into the fire at the tournament fields of the potlatch; the Californian chief, by contrast, burned calories in the closed seclusion of his sweat lodge*
### Page 256 @ 03 May 2023
*'society' refers to the mutual creation of human beings,*
### Page 256 @ 03 May 2023
*The ideal individual was both wealthy and industrious. In the first grey haze of dawn he arose to begin his day's work, never ceasing activity until late at night. Early rising and the ability to go without sleep were great virtues. It was extremely complimentary to say "he doesn't know how to sleep."'68 Wealthy men – and it should be noted that all these societies were decidedly patriarchal – were typically seen as providers for poorer dependants, improvident folk and foolish drifters, by virtue of their own self-discipline and labour and that of their wives*
### Page 257 @ 03 May 2023
*As he 'accumulates' himself and becomes cleaner, the person in training sees himself as more and more 'real' and thus the world as more and more 'beautiful': a real place in experience rather than merely a setting for a 'story,'*
### Page 257 @ 03 May 2023
*the process by which cultures define themselves against one another is always, at root, political, since it involves self-conscious arguments about the proper way to live*
### Page 258 @ 03 May 2023
*Captives were not slaves, all sources insist they were redeemed quickly, and all killers had to pay compensation; but all this required money. This meant the important men who often instigated wars could profit handsomely from the affair by lending to those unable to pay, and the latter were thus either reduced to debt peons, or retreated to live ignominiously in isolated homesteads in the woods.*
### Page 259 @ 03 May 2023
*Elsewhere in California, formal chiefs or headmen existed, and though they wielded no power of compulsion they settled conflicts by raising funds for compensation collectively, and the focus of cultural life was less on the accumulation of property than on organizing annual rites of world renewal.*
### Page 259 @ 03 May 2023
*Environmental determinists have an unfortunate tendency to treat humans as little more than automata, living out some economist's fantasy of rational calculation.*
### Page 259 @ 03 May 2023
*Those who don't follow an optimal pathway for the use of resources are destined for the ash heap of history*
### Page 260 @ 03 May 2023
*culture, but ultimately this comes down to little more than insisting that explanation is impossible: English people act the way they do because they are English, Yurok act the way they do because they're Yurok; why they are English or Yurok is not really ours to say*
### Page 260 @ 03 May 2023
*Putting matters in such stark terms does not mean there is no truth to either position. The intersection of environment and technology does make a difference, often a huge difference, and to some degree, cultural difference really is just an arbitrary roll of the dice: there's no 'explanation' for why Chinese is a tonal language and Finnish an agglutinative one; that's just the way things happened to turn out*
### Page 260 @ 03 May 2023
*one treats the arbitrariness of linguistic difference as the foundation of all social theory – which is basically what structuralism did, and post-structuralism continues to do – the result is just as mechanically deterministic as the most extreme form of environmental determination. 'Language speaks us.' We are doomed to endlessly enact patterns of behaviour not of our own creation; not of anyone's creation really, until some seismic shift in the cultural equivalent of tectonic plates lands us somehow in a new, equally inexplicable arrangement.
In other words, both approaches presume that we are already, effectively, stuck*
### Page 260 @ 03 May 2023
*Once again, our intention is simply to treat those who created these forms of culture as intelligent adults, capable of reflecting on the social worlds they were building or rejecting.*
### Page 261 @ 03 May 2023
*Marx put it best: we make our own history, but not under conditions of our own choosing*
### Page 261 @ 03 May 2023
*one reason social theorists will always be debating this issue is that we can't really know how much difference 'human agency' – the preferred term, currently, for what used to be called 'free will' – really makes.*
### Page 261 @ 03 May 2023
*It seems part of the human condition that while we cannot predict future events, as soon as those events do happen we find it hard to see them as anything but inevitable*
### Page 261 @ 03 May 2023
*Rather than defining the indigenous inhabitants of the Pacific Coast of North America as 'incipient' farmers or as examples of 'emerging' complexity – which is really just an updated way of saying they were all 'rushing headlong for their chains' – we have explored the possibility that they might have been proceeding with (more or less) open eyes, and found plenty of evidence to support it.*
### Page 262 @ 03 May 2023
*Slavery, we've argued, became commonplace on the Northwest Coast largely because an ambitious aristocracy found itself unable to reduce its free subjects to a dependable workforce.*
### Page 262 @ 03 May 2023
*A schismogenetic process ensued, whereby coastal peoples came to define themselves increasingly against each other. This was by no means just an argument about slavery; it appears to have affected everything from the configuration of households, law, ritual and art to conceptions of what it meant to be an admirable human being, and was most evident in contrasting attitudes to work, food and material wealth.*
### Page 263 @ 03 May 2023
*Slavery finds its origins in war. But everywhere we encounter it slavery is also, at first, a domestic institution. Hierarchy and property may derive from notions of the sacred, but the most brutal forms of exploitation have their origins in the most intimate of social relations: as perversions of nurture, love and caring.*
### Page 263 @ 03 May 2023
*Finally, all this suggests that, historically speaking, hierarchy and equality tend to emerge together, as complements to one another*
## 6 Gardens of Adonis: The Revolution that Never Happened: how Neolithic Peoples Avoided Agriculture
### Page 266 @ 03 May 2023
*the dog days of summer, when nothing can grow, the women of ancient Athens fashioned these little gardens in baskets and pots. Each held a mix of quick-sprouting grain and herbs. The makeshift seedbeds were carried up ladders on to the flat roofs of private houses and left to wilt in the sun: a botanical re-enactment of the premature death of Adonis, the fallen hunter, slain in his prime by a wild boar*
### Page 267 @ 03 May 2023
*Was farming from the very beginning about the serious business of producing more food to supply growing populations? Most scholars assume, as a matter of course, that this had to be the principal reason for its invention. But maybe farming began as a more playful or even subversive kind of process – or perhaps even as a side effect of other concerns, such as the desire to spend longer in particular kinds of locations, where hunting and trading were the real priorities.*
### Page 268 @ 03 May 2023
*Many rooms also had vivid wall paintings and figurative mouldings, and contained platforms under which resided some portion of the household dead – remains of between six and sixty individuals in any given house – propping up the living. We can't help recalling Maurice Sendak's vision of a magical house where 'the walls became the world all around'.*
### Page 269 @ 03 May 2023
*surprises, which oblige us to revise both the history of the world's oldest town and also how we think about the origins of farming in general. The cattle, it turns out, were not domestic: those impressive skulls belonged to fierce, wild aurochs. The shrines were not shrines, but houses in which people engaged in such everyday tasks as cooking, eating and crafts – just like anywhere else, except they happened to contain a larger density of ritual paraphernalia*
### Page 270 @ 03 May 2023
*Today, most archaeologists consider it deeply unsound to interpret prehistoric images of corpulent women as 'fertility goddesses'. The very idea that they should be is the result of long-outmoded Victorian fantasies about 'primitive matriarchy'.*
### Page 270 @ 03 May 2023
*In the same way that social rebels, since the 1960s, tended to idealize hunter-gatherer bands, earlier generations of poets, anarchists and bohemians had tended to idealize the Neolithic as an imaginary, beneficent theocracy ruled over by priestesses of the Great Goddess, the all-powerful distant ancestor of Inanna, Ishtar, Astarte and Demeter herself – that is, until such societies were overwhelmed by violent, patriarchal Indo-European-speaking horse-men descending from the steppes, or, in the case of the Middle East, Semitic-speaking nomads from the deserts. How people saw this imagined confrontation became the source of a major political divide in the late nineteenth and early twentieth centuries.*
### Page 272 @ 03 May 2023
*Among academics today, belief in primitive matriarchy is treated as a kind of intellectual offence, almost on a par with 'scientific racism', and its exponents have been written out of history: Gage from the history of feminism, Gross from that of psychology (despite inventing such concepts as introversion and extroversion, and having worked closely with everyone from Franz Kafka and the Berlin Dadaists to Max Weber).*
### Page 272 @ 03 May 2023
*it has created a situation where scholars find it difficult even to speculate as to how hierarchy and exploitation came to take root in the domestic sphere – unless one wants to return to Rousseau, and the simplistic notion that settled farming somehow automatically generated the power of husbands over wives and fathers over children*
### Page 273 @ 03 May 2023
*The older version was rooted in an evolutionary anthropology that assumed matriarchy was the original condition of humankind because, at first, people supposedly didn't understand physiological paternity and assumed women were single-handedly responsible for producing babies.*
### Page 274 @ 03 May 2023
*Gimbutas, though, was not proposing anything of this sort: she was arguing for women's autonomy and ritual priority in the Middle Eastern and European Neolithic. Yet by the 1990s many of her ideas had become a charter for ecofeminists, New Age religions and a host of other social movements; in turn, they inspired a slew of popular books, ranging from the philosophical to the ridiculous – and in the process became entangled with some of the more extravagant older Victorian ideas*
### Page 274 @ 03 May 2023
*Before long, she was being accused of just about everything the academy could think to throw at her: from cherry-picking evidence to failing to keep up with methodological advances; accusations of reverse sexism; or that she was indulging in 'myth-making'. She was even subject to the supreme insult of public psychoanalysis, as leading academic journals published articles suggesting her theories about the displacement of Old Europe were basically phantasmagorical projections of her own tumultuous life experience,*
### Page 274 @ 03 May 2023
*Gimbutas's arguments involved myth-making of a sort, which in part explains this wholesale takedown of her work by the academic community. But when male scholars engage in similar myth-making – and, as we have seen, they frequently do – they not only go unchallenged but often win prestigious literary prizes and have honorary lectures created in their name.*
### Page 276 @ 03 May 2023
*Matriarchy might refer to an equivalent situation, in which the role of mothers in the household similarly becomes a model for, and economic basis of, female authority in other aspects of life (which doesn't necessarily imply dominance in a violent or exclusionary sense), where women as a result hold a preponderance of overall day-to-day power.*
### Page 277 @ 03 May 2023
*True, such matriarchal arrangements are somewhat unusual – at least in the ethnographic record, which covers roughly the last 200 years. But once it's clear that such arrangements can exist, we have no particular reason to exclude the possibility that they were more common in Neolithic times,*
### Page 279 @ 03 May 2023
*The residents of Çatalhöyük seem to have placed great value on routine. We see this most clearly in the fastidious reproduction of domestic layouts over time. Individual houses were typically in use for between fifty and 100 years, after which they were carefully dismantled and filled in to make foundations for superseding houses.*
### Page 280 @ 03 May 2023
*The authority of long-lived houses seems consistent with the idea that elders, and perhaps elder women in particular, held positions of influence. But the more prestigious households are distributed among the less, and do not coalesce into elite neighbourhoods.*
### Page 280 @ 03 May 2023
*In pictorial art, masculine themes do not encompass the feminine, nor vice versa. If anything, the two domains seem to be kept apart, in different sectors of dwellings*
### Page 280 @ 03 May 2023
*it is clear that house floors were regularly swept clean, so the distribution of artefacts around them is far from a straightforward representation of past activities*
### Page 281 @ 03 May 2023
*Traces have also been found of reed mats that covered living surfaces and furnishings, further disturbing the picture. We don't necessarily know everything that was happening in the houses, or perhaps even half of it – or, indeed, how much time was actually spent living in these cramped and peculiar structures at all*
### Page 281 @ 03 May 2023
*fact quite likely that what we are seeing in the surviving remains of Çatalhöyük's built environment are largely the social arrangements prevalent in winter, with their intense and distinctive ceremonialism focused upon hunting and the veneration of the dead*
### Page 283 @ 03 May 2023
*It was from that general direction that the founders of Çatalhöyük obtained the basis of their farming economy, including domestic cereals, pulses, sheep and goats. But they didn't adopt domestic cattle or pigs. Why not?
Since no environmental obstacles were present, one has to assume an element of cultural refusal here. The best contender for an explanation is also the most obvious. As Çatalhöyük's art and ritual suggests, wild cattle and boar were highly valued as prey*
### Page 283 @ 03 May 2023
*what exactly is, or was, the Fertile Crescent? First, it's important to note that this is a completely modern concept, the origins of which are as much geopolitical as environmental. The term Fertile Crescent was invented in the nineteenth century, when Europe's imperial powers were carving up the Middle East according to their own strategic interests. Partly because of the close ties between archaeology, ancient history and the modern institutions of empire, the term became widely adopted among researchers to describe an area from the eastern shores of the Mediterranean (modern Palestine, Israel and Lebanon) to the foothills of the Zagros Mountains (roughly the Iran–Iraq border), crossing parts of Syria, Turkey and Iraq on the way. Now it is only prehistorians who still use it, to indicate the region where farming began: a roughly crescent-shaped belt of arable lands bounded by deserts and mountains.*
### Page 284 @ 04 May 2023
*Between 10,000 and 8000 BC, foraging societies in the 'upland' and 'lowland' sectors of the Fertile Crescent underwent marked transformations, but in quite different directions.*
### Page 285 @ 04 May 2023
*It might even be argued that, after the last Ice Age, the ecological frontier between 'lowland' and 'upland' Fertile Crescent also became a cultural frontier with zones of relative uniformity on either side, distinguished almost as sharply as the 'Protestant foragers' and 'fisher kings' of the Pacific Coast*
### Page 285 @ 04 May 2023
*Obsidian from the Turkish highlands flowed south, and shells (perhaps used as currency) flowed north from the shores of the Red Sea, ensuring that uplanders and lowlanders stayed in touch.*
### Page 286 @ 04 May 2023
*lowlanders who lived here were devoted craft specialists and traders. Each hamlet seems to have developed its own expertise (stone-grinding, bead-carving, shell-processing and so on), and industries were often associated with special 'cult buildings' or seasonal lodges, pointing to the control of such skills by guilds or secret societies*
### Page 286 @ 04 May 2023
*Lowland foragers occupied fertile pockets of land among the drainages of the Jordan valley, using trade wealth to support increasingly large, settled populations.*
### Page 286 @ 04 May 2023
*There were constant opportunities for foragers to exchange complementary products – which included foods, medicines, drugs and cosmetics – since the local growth cycles of wild resources were staggered by sharp differences in climate and topography.*
### Page 289 @ 04 May 2023
*At higher altitudes, in the upland crescent, we find some of the earliest evidence for the management of livestock (sheep and goats in western Iran, cattle too in eastern Anatolia), incorporated into seasonal rounds of hunting and foraging*
### Page 289 @ 04 May 2023
*Cultivation, however, is rarely just about calories. Cereal production also brought people together in new ways to perform communal tasks, mostly repetitive, labour-intensive and no doubt freighted with symbolic meaning;*
### Page 289 @ 04 May 2023
*the storage and processing of grain was associated less with ordinary dwellings than with subterranean lodges, entered from an opening in the roof and suffused with ritual associations*
### Page 289 @ 04 May 2023
*In crops, domestication is what happens when plants under cultivation lose features that allow them to reproduce in the wild. Among the most important is the facility to disperse seed without human assistance*
### Page 290 @ 04 May 2023
*So how did these genetic and behavioural changes in crops come about, how long did it take, and what had to happen in human societies to make them possible?*
### Page 291 @ 04 May 2023
*ours is a species that has become enslaved to its crops: wheat, rice, millet and corn feed the world, and it's hard to envisage modern life without them.*
### Page 291 @ 04 May 2023
*to make sense of the beginnings of Neolithic farming, we surely need to try and see it from the perspective of the Palaeolithic, not of the present*
### Page 291 @ 04 May 2023
*We already know how this one goes. Humans were once living a 'fairly comfortable life', subsisting from the blessings of Nature, but then we made our most fatal mistake. Lured by the prospect of a still easier life – of surplus and luxury, of living like gods – we had to go and tamper with that harmonious State of Nature, and thus unwittingly turned ourselves into slaves*
### Page 292 @ 04 May 2023
*Once cultivation became widespread in Neolithic societies, we might expect to find evidence of a relatively quick or at least continuous transition from wild to domestic forms of cereals (which is exactly what terms like the 'Agricultural Revolution' lead us to think), but in fact this is not at all what the results of archaeological science show.*
### Page 292 @ 04 May 2023
*All it would have taken, then, is for humans to follow the cues provided by the crops themselves. That meant harvesting after they began to ripen, doing it in ways that left the grain on the stem (e.g. cutting or pulling, as opposed to beating grain straight off the ear with a paddle), sowing new seed on virgin soil (away from wild competitors), learning from errors, and repeating the winning formula*
### Page 293 @ 04 May 2023
*Harvesting by sickle yields straw as well as grain. Today we consider straw a by-product of cereal-farming, the primary purpose being to produce food. But archaeological evidence suggests things started the other way round.*
### Page 293 @ 04 May 2023
*Now here's the key point: if crops, rather than humans, had been setting the pace, these two processes would have gone hand in hand,*
### Page 293 @ 04 May 2023
*Within a few human generations, the Faustian pact between people and crops would have been sealed. But here again, the evidence flatly contradicts these expectations.*
### Page 294 @ 04 May 2023
*fact, the latest research shows that the process of plant domestication in the Fertile Crescent was not fully completed until much later: as much as 3,000 years after the cultivation of wild cereals first began*
### Page 294 @ 04 May 2023
*'a few short millennia' here or there, we can hardly extend this attitude to the prehistoric actors whose lives we are trying to understand*
### Page 295 @ 04 May 2023
*To be clear: that's 3,000 years of human history, far too long to constitute an 'Agricultural Revolution' or even to be considered some kind of transitional state on the road to farming*
### Page 295 @ 04 May 2023
*Serious farming meant serious soil maintenance and weed clearance. It meant threshing and winnowing after harvest. All these activities would have got in the way of hunting, wild food collection, craft production, marriages and any number of other things*
### Page 295 @ 04 May 2023
*Indeed, to balance out their dietary needs and labour costs, early cultivators may even have strategically chosen practices that worked against the morphological changes which signal the onset of domestication in plants*
### Page 296 @ 04 May 2023
*which brings us back full circle to Çatalhöyük and its wetland location. Called 'flood retreat', 'flood recession' or décrue farming, it takes place on the margins of seasonally flooding lakes or rivers. Flood-retreat farming is a distinctly lackadaisical way to raise crops. The work of soil preparation is given over mostly to nature. Seasonal flooding does the work of tillage, annually sifting and refreshing the soil. As the waters recede they leave behind a fertile bed of alluvial earth, where seed can be broadcast.*
### Page 297 @ 14 May 2023
*Rejecting a Garden of Eden-type narrative for the origins of farming also means rejecting, or at least questioning, the gendered assumptions lurking behind that narrative.42*
### Page 332 @ 14 May 2023
*When today's writers speculate about 'wheat domesticating humans' (as opposed to 'humans domesticating wheat'), what they are really doing is replacing a question about concrete scientific (human) achievements with something rather more mystical.*
### Page 332 @ 14 May 2023
*find ourselves waxing lyrical about the temptations of forbidden fruits and musing on the unforeseen consequences of adopting a technology (agriculture) that Jared Diamond has characterized – again, with biblical overtones – as 'the worst mistake in the history of the human race*
### Page 333 @ 14 May 2023
*where evidence exists, it points to strong associations between women and plant-based knowledge as far back as one can trace such things*
### Page 334 @ 14 May 2023
*What if we shifted the emphasis away from agriculture and domestication to, say, botany or even gardening? At once we find ourselves closer to the realities of Neolithic ecology, which seem little concerned with taming wild nature or squeezing as many calories as possible from a handful of seed grasses. What it really seems to have been about is creating garden plots – artificial, often temporary habitats – in which the ecological scales were tipped in favour of preferred species.*
### Page 335 @ 14 May 2023
*Theirs was not a science of domination and classification, but one of bending and coaxing, nurturing and cajoling, or even tricking the forces of nature, to increase the likelihood of securing a favourable outcome*
### Page 337 @ 14 May 2023
*As we'll soon see, earth and clay even came to redefine relationships between the living and the dead.*
### Page 338 @ 14 May 2023
*Seen this way, the 'origins of farming' start to look less like an economic transition and more like a media revolution, which was also a social revolution, encompassing everything from horticulture to architecture, mathematics to thermodynamics, and from religion to the remodelling of gender roles. And while we can't know exactly who was doing what in this brave*
### Page 338 @ 14 May 2023
*these communities did not develop in isolation. For almost the entire period we've been discussing, the upland crescent – following the foothills of the Taurus and Zagros Mountains and the adjoining steppe – was also home to settled populations*
### Page 338 @ 14 May 2023
*in other ways they are clearly marked out from their lowland neighbours, their construction of megalithic architecture, including the famous structures of Göbekli Tepe, being just the most obvious. Some of these groups lived in proximity to lowland Neolithic societies, especially along the upper reaches of the Euphrates, but their art and ritual suggest a radically different orientation to the world*
### Page 340 @ 14 May 2023
*archaeologists remain rightly cautious about linking such practices to conflict or predation; so far, there is only limited evidence for interpersonal violence, let alone warfare at this time.*
### Page 342 @ 14 May 2023
*hunting as predation, shifting subtly from a mode of subsistence to a way of modelling and enacting dominance over other human beings. After all, even feudal lords in Europe tended to identify themselves with lions, hawks and predatory beasts*
### Page 343 @ 14 May 2023
*In*
### Page 343 @ 14 May 2023
*Neolithic farming began in Southwest Asia as a series of local specializations in crop-raising and animal-herding, scattered across various parts of the region, with no epicentre*
### Page 344 @ 14 May 2023
*But from its earliest beginnings, farming was much more than a new economy. It also saw the creation of patterns of life and ritual that remain doggedly with us millennia later, and have since become fixtures of social existence among a broad sector of humanity:*
### Page 344 @ 14 May 2023
*much of this Neolithic lifestyle developed alongside an alternative cultural pattern in the steppe and upland zones of the Fertile Crescent, most clearly distinguished by the building of grand monuments in stone, and by a symbolism of male virility*
### Page 344 @ 14 May 2023
*By contrast, the art and ritual of lowland settlements in the Euphrates and Jordan valleys presents women as co-creators of a distinct form of society*
### Page 344 @ 14 May 2023
*Of course, we could put these cultural oppositions down to coincidence, or perhaps even environmental factors. But considering the close proximity of the two cultural patterns, and how the groups responsible for them exchanged goods and were keenly aware of each other's existence, it is equally possible, and perhaps more plausible, to see what happened as the result of mutual and self-conscious differentiation,*
### Page 348 @ 14 May 2023
*In the Fertile Crescent of the Middle East, long regarded as the cradle of the 'Agricultural Revolution', there was in fact no 'switch' from Palaeolithic forager to Neolithic farmer.*
### Page 348 @ 14 May 2023
*while agriculture allowed for the possibility of more unequal concentrations of wealth, in most cases this only began to happen millennia after its inception*
### Page 348 @ 14 May 2023
*the Fertile Crescent, it is – if anything – among upland groups, furthest removed from a dependence on agriculture, that we find stratification and violence becoming entrenched; while their lowland counterparts, who linked the production of crops to important social rituals, come out looking decidedly more egalitarian*
### Page 349 @ 14 May 2023
*if the adoption of farming actually set humanity, or some small part of it, on a course away from violent domination, what went wrong*
## 7 The Ecology of Freedom: How Farming First Hopped, Stumbled and Bluffed Its way around the World
### Page 352 @ 14 May 2023
*it no longer makes sense to ask, 'what were the social implications of the transition to farming?' – as if there was necessarily just one transition, and one set of implications*
### Page 352 @ 14 May 2023
*Communal tenure, 'open-field' principles, periodic redistribution of plots and co-operative management of pasture are not particularly exceptional*
### Page 352 @ 14 May 2023
*The Russian mir is a famous example, but similar systems of land redistribution once existed all over Europe, from the Highlands of Scotland to the Balkans*
### Page 353 @ 14 May 2023
*the precise location of each strip was determined by lottery, with each family receiving one strip per land tract of differing quality, so that nobody was obliged to travel much further than anyone else to his fields or to work soil of consistently lower quality*
### Page 353 @ 14 May 2023
*'cases were frequent in which the arable land was divided into farms which shifted among the tenant-families periodically, and sometimes annually.'*
### Page 354 @ 14 May 2023
*Geographers and historians used to believe that plants and animals were first domesticated in just a few 'nuclear' zones: the same areas in which large-scale, politically centralized societies later appeared.*
### Page 354 @ 14 May 2023
*Such neat geographical alignments between early centres of crop domestication and the rise of centralized states invited speculation that the former led to the latter: that food production was responsible for the emergence of cities, writing, and centralized political organization,*
### Page 355 @ 14 May 2023
*Experts now identify between fifteen and twenty independent centres of domestication, many of which followed very different paths of development*
### Page 355 @ 14 May 2023
*Food production did not always present itself to foragers, fishers and hunters as an obviously beneficial thing.*
### Page 355 @ 14 May 2023
*Jared Diamond:
Just as some regions proved much more suitable than others for origins of food production, the ease of its spread also varied greatly around the world. Some areas that are ecologically very suitable for food production never acquired it in prehistoric times at all, even though areas of prehistoric food production existed nearby*
### Page 359 @ 14 May 2023
*This is just an updated version of the old diffusionist approach, which identifies culture traits (cat's cradles, musical instruments, agriculture and so on) and maps out how they migrate across the globe,*
### Page 359 @ 14 May 2023
*Even in the American Southwest, the overall trend for 500 years or so before Europeans arrived was the gradual abandonment of maize and beans, which people had been growing in some cases for thousands of years, and a return to a foraging way of life*
### Page 360 @ 14 May 2023
*books on world history, you often encounter phrases like 'crops and livestock spread rapidly through Eurasia', or 'the plant package of the Fertile Crescent launched food production from Ireland to the Indus*
### Page 360 @ 14 May 2023
*If anything, it seems to reflect the experience of the last few centuries, when Old World domesticates did indeed conquer the environments of the Americas and Oceania*
### Page 360 @ 14 May 2023
*seeds can spread very quickly if those carrying them have an army and are driven by the need endlessly to expand their enterprises to maintain profits.*
### Page 360 @ 14 May 2023
*domestic plants and animals could not 'spread' beyond their original ecological limits without significant effort on the part of their human planters and keepers.*
### Page 361 @ 14 May 2023
*Eurasia, as these authors point out, has few equivalents to the sharp climatic variations of the Americas, or indeed of Africa. Terrestrial species can travel across the breadth of the Eurasian continent without crossing boundaries between tropical and temperate zones.*
### Page 362 @ 14 May 2023
*To what extent can such observations help to make sense of human history on a larger scale? How far can geography go in explaining history, rather than simply informing it*
### Page 362 @ 14 May 2023
*went on to argue that the global ascendance of European economies since the sixteenth century could be accounted for by a process he called 'ecological imperialism*
### Page 362 @ 14 May 2023
*temperate zones of North America and Oceania*
### Page 362 @ 14 May 2023
*were ideally suited to Eurasian crops and livestock; not only because of their climate, but because they possessed few native competitors and no local parasites*
### Page 362 @ 14 May 2023
*Unleashed on such fresh environments, Old World domesticates went into reproductive overdrive, even going feral again in some cases. Outgrowing and out-grazing local flora and fauna, they began to turn native ecosystems on their heads, creating 'Neo-Europes' – carbon copies of European environments,*
### Page 363 @ 14 May 2023
*While European plants thrived in the absence of pests, diseases brought with domestic animals (or by humans accustomed to living alongside them) wreaked havoc on indigenous populations, creating casualty rates as high as 95 per cent, even in places where settlers were not enslaving or actively massacring the indigenous population – which, of course, they often were*
### Page 363 @ 14 May 2023
*why is our discussion of these issues confined only to the last 10,000 or so years of human history? Given that humans have been around for upwards of 200,000 years, why didn't farming develop much earlier?*
### Page 363 @ 14 May 2023
*Since our species came into existence, there have been only two sustained periods of warm climate of the kind that might support an agricultural economy for long enough to leave some trace in the archaeological record.11 The first was the Eemian interglacial, which took place around 130,000 years ago.*
### Page 364 @ 14 May 2023
*The second is the one we are living in now. When it began, around 12,000 years ago, people were already present on all the world's continents, and in many different kinds of environment. Geologists call this period the Holocene, from Greek holos (entire), kainos (new).*
### Page 364 @ 14 May 2023
*at least the last two centuries we have been entering a new geological epoch, the Anthropocene, in which for the first time in history human activities are the main drivers of global climate change*
### Page 364 @ 14 May 2023
*With perhaps 90 per cent of the indigenous population eliminated by the effects of conquest and infectious disease, forests reclaimed regions in which terraced agriculture and irrigation had been practised for centuries. In Mesoamerica, Amazonia and the Andes, some 50 million hectares of cultivated land may have reverted to wilderness. Carbon uptake from vegetation increased on a scale sufficient to change the Earth System and bring about a human-driven phase of global cooling.*
### Page 364 @ 14 May 2023
*the Anthropocene is what we have done with the legacy of a Holocene Age, which in some ways had been a 'clean sheet' for humanity.*
### Page 365 @ 14 May 2023
*For many historians, the onset of the Holocene is significant because it created conditions for the origins of agriculture. Yet in many parts of the world, as we've already seen, it was also a Golden Age for foragers*
### Page 365 @ 14 May 2023
*The most vigorous expansion of foraging populations was in coastal environments, freshly exposed by glacial retreat. Such locations offered a bonanza of wild resources.*
### Page 365 @ 14 May 2023
*foragers used various techniques of land management to stimulate the growth of desired species, such as fruit and nut-bearing trees. By 8000 BC, their efforts had contributed to the extinction of roughly two-thirds of the world's megafauna,*
### Page 366 @ 14 May 2023
*Where forest took over from steppe, human hunting techniques shifted from the seasonal co-ordination of mass kills to more opportunistic and versatile strategies, focused on smaller mammals with more limited home ranges,*
### Page 366 @ 14 May 2023
*farmers entered into this whole new world very much as the cultural underdogs*
### Page 366 @ 14 May 2023
*Mostly, as we'll see, they filled in the territorial gaps left behind by foragers: geographical spaces either too remote, inaccessible or simply undesirable to attract the sustained attention of hunters, fishers and gatherers.*
### Page 366 @ 14 May 2023
*the fate of early farming societies often hinged less on 'ecological imperialism' than on what we might call – to adapt a phrase from the pioneer of social ecology, Murray Bookchin – an 'ecology of freedom'.17*
### Page 367 @ 14 May 2023
*The ecology of freedom describes the proclivity of human societies to move (freely) in and out of farming; to farm without fully becoming farmers; raise crops and animals without surrendering too much of one's existence to the logistical rigours of agriculture;*
### Page 367 @ 14 May 2023
*Moving freely in and out of farming in this way, or hovering on its threshold, turns out to be something our species has done successfully for a large part of its past.19 Such fluid ecological arrangements*
### Page 367 @ 14 May 2023
*were once typical of human societies in many parts of the world.*
### Page 368 @ 14 May 2023
*Biodiversity – not bio-power – was the initial key to the growth of Neolithic food production.*
### Page 369 @ 14 May 2023
*The arrival of farming in central Europe was associated with an initial and quite massive upsurge in population – which is of course exactly what one would expect. But what followed was not the anticipated 'up and up' pattern of demographic growth. Instead came a disastrous downturn, a boom and bust, between 5000 and 4500 BC, and something approaching a regional collapse.*
### Page 369 @ 14 May 2023
*Older narratives of prehistory tended simply to assume that Neolithic colonists held the upper hand over native foraging populations, demographically and socially; that they either replaced them, or converted them to a superior way of life*
### Page 370 @ 14 May 2023
*The boom-and-bust pattern of early farming now documented in temperate Europe contradicts this picture and raises wider questions about the viability of Neolithic economies*
### Page 372 @ 14 May 2023
*Around the time that Linear Pottery settlements were established in central Europe, the Neolithic farming economy made its first appearance in Africa.*
### Page 372 @ 14 May 2023
*These first African farmers reinvented the Neolithic in their own image. Cereal cultivation was relegated to a minor pursuit (regaining its status only centuries later), and the idea that one's social identity was represented by hearth and home was largely thrown out too.*
### Page 373 @ 14 May 2023
*Herders moved periodically in and out of this 'Green Sahara', both west and east to the Red Sea coast. Complex systems of bodily display developed. New forms of personal adornment employed cosmetic pigments and minerals, prospected from the adjacent deserts, and a dazzling array of beadwork, combs, bangles and other ornaments made of ivory and bone,*
### Page 373 @ 14 May 2023
*before there were pharaohs – almost anyone could hope to be buried like a king, queen, prince or princess.*
### Page 374 @ 14 May 2023
*Like the Linear Pottery farmers of central Europe, Lapita groups seem to have avoided established centres of population.*
### Page 374 @ 14 May 2023
*Voyaging eastwards, Lapita peoples left a trail of distinctive pottery, their most consistent signal in the archaeological record.*
### Page 375 @ 14 May 2023
*More recent traditions of Polynesian tattooing and body art – 'wrapping the body in images', as a famous anthropological study puts it – remind us how little we really know of the vibrant conceptual worlds of earlier times, and those who first carried such practices across remote Pacific island-scapes.*
### Page 375 @ 14 May 2023
*first inspection, these three variations on 'the Neolithic' – European, African, Oceanic – might seem to have almost nothing in common. However, all share two important features. First, each involved a serious commitment to farming*
### Page 375 @ 14 May 2023
*the Linear Pottery culture of Europe enmeshed itself most deeply in the raising of cereals and livestock. The Nile valley was fully wedded to its herds, as was the Lapita to its pigs and yams.*
### Page 376 @ 14 May 2023
*Second, all three cases involved a targeted spread of farming to lands largely uninhabited by existing populations.*
### Page 376 @ 14 May 2023
*But not all early farming expansions were of this 'serious' variety. In the lowland tropics of South America, archaeological research has uncovered a distinctly more playful tradition*
### Page 376 @ 14 May 2023
*found among the Nambikwara of Brazil's Mato Grosso region*
### Page 376 @ 14 May 2023
*Well into the twentieth century, they spent the rainy season in riverside villages, clearing gardens and orchards to grow a panoply of crops including sweet and bitter manioc, maize, tobacco, beans, cotton, groundnuts, gourds and more besides. Cultivation was a relaxed affair, with little effort spent on keeping different species apart*
### Page 377 @ 14 May 2023
*Many rainforest groups carry with them what can only be described as a small zoo comprising tamed forest creatures: monkeys, parrots, collared peccaries, and so on.*
### Page 377 @ 14 May 2023
*Pets are not eaten. Nor are their keepers interested in breeding them. They live as individual members of the community, who treat them much like children, as subjects of affection and sources of amusement.*
### Page 377 @ 14 May 2023
*In Amazonia, what this means in practice is that people avoid intervening in the reproduction of those particular species lest they usurp the role of spirits.*
### Page 378 @ 14 May 2023
*Amazonia shows how this 'in-and-out-of-farming' game could be far more than a transient affair. It seems to have played out over thousands of years, since during that time there is evidence of plant domestication and land management, but little commitment to agriculture.*
### Page 339 @ 14 May 2023
*Over 2,000 years ago, a similar process of strategic cultural mixing (quite unlike the avoidance strategies of more 'serious' farmers) seems to have brought about the convergence of the Amazon basin into a regional system*
### Page 340 @ 14 May 2023
*We now know that, by the beginning of the Christian era, the Amazonian landscape was already studded with towns, terraces, monuments and roadways, reaching all the way from the highland kingdoms of Peru to the Caribbean.*
### Page 341 @ 14 May 2023
*All this implies that at least some early inhabitants of Amazonia were well aware of plant domestication but did not select it as the basis of their economy, opting instead for a more flexible kind of agroforestry*
### Page 342 @ 14 May 2023
*loose and flexible patterns of food production sustained civilizational growth on a continent-wide scale, long before Europeans arrived*
### Page 342 @ 14 May 2023
*fully domesticated rice strains only appear fifteen centuries after the first cultivation of wild rice in paddy fields. It might have even taken longer were it not for a snap of global cooling around 5000 BC, which depleted wild rice stands and nut harvests.*
## 8 Imaginary Cities: Eurasia's First Urbanites – in Mesopotamia, the Indus Valley, Ukraine and China – and how They Built Cities without Kings
### Page 348 @ 25 May 2023
*Very large social units are always, in a sense, imaginary. Or, to put it in a slightly different way: there is always a fundamental distinction between the way one relates to friends, family, neighbourhood, people and places that we actually know directly, and the way one relates to empires, nations and metropolises, phenomena that exist largely, or at least most of the time, in our heads.*
### Page 349 @ 25 May 2023
*We're designed to work in small teams. As a result, large agglomerations of people are often treated as if they were by definition somewhat unnatural, and humans as psychologically ill equipped to handle life inside them. This is the reason, the argument often goes, that we require such elaborate 'scaffolding' to make larger communities work: such things as urban planners, social workers, tax auditors and police.*
### Page 350 @ 25 May 2023
*In many early cities, there is simply no evidence of either a class of administrators or any other sort of ruling stratum. In others, centralized power seems to appear and then disappear. It would seem that the mere fact of urban life does not, necessarily, imply any particular form of political organization, and never did.*
### Page 350 @ 25 May 2023
*'Common sense' is a peculiar expression. Sometimes it means exactly what it seems to mean: practical wisdom born of real-life experience, avoiding stupid, obvious pitfalls.*
### Page 350 @ 25 May 2023
*On the other hand, it occasionally turns out that things which seem like simple common sense are, in fact, not.*
### Page 350 @ 25 May 2023
*it is almost universal common sense that it's relatively easy for a small group to treat each other as equals and come to decisions democratically, but that the larger the number of people involved, the more difficult this becomes.*
### Page 353 @ 25 May 2023
*It would seem, then, that kinship in such cases is really a kind of metaphor for social attachments, in much the same way we'd say 'all men are brothers' when trying to express internationalism*
### Page 354 @ 25 May 2023
*cities have a life that transcends all this. This is not because of the permanence of stone or brick or adobe; neither is it because most people in a city actually meet one another. It is because they will often think and act as people who belong to the city – as Londoners or Muscovites or Calcuttans*
### Page 354 @ 25 May 2023
*urbanites live in small social worlds that touch but do not interpenetrate.*
### Page 354 @ 25 May 2023
*Aristotle, for example, insisted that Babylon was so large that, two or three days after it had been captured by a foreign army, some parts of the city still hadn't heard the news.*
### Page 355 @ 25 May 2023
*Living in unbounded, eternal, largely imaginary groups is effectively what humans had been doing all along.*
### Page 355 @ 25 May 2023
*One of the things that makes it so difficult to fit what we now know about them into an old-fashioned evolutionary sequence, where cities, states, bureaucracies and social classes all emerge together,9 is just how different these cities are. It's not just that some early cities lack class divisions, wealth monopolies, or hierarchies of administration. They exhibit such extreme variability as to imply, from the very beginning, a conscious experimentation in urban form.*
### Page 356 @ 25 May 2023
*We may never be able to reconstruct in any detail the unwritten constitutions of the world's first cities, or the upheavals that appear to have periodically changed them*
### Page 357 @ 27 May 2023
*Almost everywhere, in these early cities, we find grand, self-conscious statements of civic unity, the arrangement of built spaces in harmonious and often beautiful patterns, clearly reflecting some kind of planning at the municipal scale.*
### Page 357 @ 27 May 2023
*we find large groups of citizens referring to themselves, not in the idiom of kinship or ethnic ties, but simply as 'the people' of a given city (or often its 'sons and daughters'), united by devotion to its founding ancestors, its gods or heroes, its civic infrastructure and ritual calendar, which always involves at least some occasions for popular festivity*
### Page 357 @ 27 May 2023
*Civic festivals were moments when the imaginary structures to which people deferred in their daily lives, but which couldn't normally be seen, temporarily took on tangible, material form*
### Page 357 @ 27 May 2023
*People who lived in cities often came from far away. The great city of Teotihuacan in the Valley of Mexico was already attracting residents from such distant areas as Yucatán and the Gulf Coast in the third or fourth century*
### Page 358 @ 27 May 2023
*the largest early cities, those with the greatest populations, did not appear in Eurasia – with its many technical and logistical advantages – but in Mesoamerica, which had no wheeled vehicles or sailing ships, no animal-powered traction or transport, and much less in the way of metallurgy or literate bureaucracy*
### Page 359 @ 27 May 2023
*around 7,000 years ago, flood regimes started changing, giving way to more settled routines. This is what created wide and highly fertile floodplains along the Yellow River, the Indus, the Tigris and other rivers that we associate with the first urban civilizations.*
### Page 360 @ 27 May 2023
*We now know, for instance, that in China's Shandong province, on the lower reaches of the Yellow River, settlements of 300 hectares or more – such as Liangchengzhen and Yaowangcheng – were present by no later than 2500 BC, which is over 1,000 years before the earliest royal dynasties*
### Page 361 @ 27 May 2023
*in the valley of Peru's Rio Supe, notably at the site of Caral, where archaeologists have uncovered sunken plazas and monumental platforms four millennia older than the Inca Empire*
### Page 361 @ 27 May 2023
*Similar revelations are emerging from the Maya lowlands, where ceremonial centres of truly enormous size – and, so far, presenting no evidence of monarchy or stratification – can now be dated back as far as 1000 BC: more than 1,000 years before the rise of Classic Maya kings, whose royal cities were notably smaller in scale.*
### Page 362 @ 27 May 2023
*these settlements, often referred to as 'mega-sites' – with their modern names of Taljanky, Maidenetske, Nebelivka and so on – dated to the early and middle centuries of the fourth millennium BC, which meant that some existed even before the earliest known cities in Mesopotamia. They were also larger in area*
### Page 363 @ 27 May 2023
*No evidence was unearthed of centralized government or administration – or indeed, any form of ruling class. In other words, these enormous settlements had all the hallmarks of what evolutionists would call a 'simple', not a 'complex' society*
### Page 364 @ 27 May 2023
*The mega-sites of Ukraine and adjoining regions were inhabited from roughly 4100 to 3300 BC, that is, for something in the order of eight centuries, which is considerably longer than most subsequent urban traditions.*
### Page 364 @ 27 May 2023
*processes of soil formation on the flatlands north of the Black Sea. These black earths (Russian: chernozem) are legendary for their fertility*
### Page 364 @ 27 May 2023
*ancient Athens was largely fed by Black Sea grain).*
### Page 364 @ 27 May 2023
*The Neolithic people who settled there had travelled east from the lower reaches of the Danube, passing through the Carpathian Mountains. We do not know why, but we do know that – throughout their peregrinations in river valleys and mountain passes – they retained a cohesive social identity.*
### Page 369 @ 27 May 2023
*Mega-site dwellers were hunters of red deer, roe deer and wild boar as well as farmers and foresters. It was 'play farming' on a grand scale: an urban populus supporting itself through small-scale cultivation and herding, combined with an extraordinary array of wild foods.*
### Page 369 @ 27 May 2023
*This way of life was by no means 'simple'. As well as managing orchards, gardens, livestock and woodlands, the inhabitants of these cities imported salt in bulk from springs in the eastern Carpathians and the Black Sea littoral. Flint extraction by the ton took place in the Dniestr valley, furnishing material for tools.*
### Page 369 @ 27 May 2023
*There is no firm consensus among archaeologists about what sort of social arrangements all this required, but most would agree the logistical challenges were daunting. A surplus was definitely produced, and with it ample potential for some to seize control of the stocks and supplies, to lord it over others or battle for the spoils; but over eight centuries we find little evidence for warfare or the rise of social elites*
### Page 370 @ 27 May 2023
*beginning at the level of individual households. Each of these had a roughly common plan, but each was also, in its own way, unique.*
### Page 370 @ 27 May 2023
*It's as if every household was an artists' collective which invented its own unique aesthetic style*
### Page 371 @ 27 May 2023
*Individual households would sometimes opt to cluster together in groups of between three and ten families. Ditches or pits marked their boundaries.*
### Page 371 @ 27 May 2023
*Each had access to at least one assembly house, a structure larger than an ordinary dwelling where a wider sector of the population might gather periodically for activities we can only guess at*
### Page 371 @ 27 May 2023
*Careful analysis by archaeologists shows how the apparent uniformity of the Ukrainian mega-sites arose from the bottom up, through processes of local decision-making.*
### Page 372 @ 27 May 2023
*These modern Basque societies – tucked down in the southwest corner of France – also imagine their communities in circular form, just as they imagine themselves as being surrounded by a circle of mountains. They do so as a way of emphasizing the ideal equality of households and family units.*
### Page 372 @ 27 May 2023
*Nonetheless, they provide an excellent illustration of how such circular arrangements can form part of self-conscious egalitarian projects, in which 'everyone has neighbours to the left and neighbours to the right. No one is first, and no one is last.'*
### Page 372 @ 27 May 2023
*Each Sunday, one household will bless two loaves at the local church, eat one, then present the other to its 'first neighbour' (the house to their right); the next week that neighbour will do the same to the next house to its right, and so on in a clockwise direction, so that in a community of 100 households it would take about two years to complete a full cycle.*
### Page 372 @ 27 May 2023
*meanwhile, care for the dead and dying travels in the opposite, counter-clockwise direction.*
### Page 373 @ 27 May 2023
*It follows that households cannot simply schedule their daily labour in line with their own needs. They also have to consider their obligations to other households, which in turn have their own obligations to other, different households, and so on. Factoring in that some tasks – such as moving flocks to highland pastures, or the demands of milking, shearing and guarding herds – may require the combined efforts of ten different households, and that households have to balance the scheduling of numerous different sorts of commitment, we begin to get a sense of the complexities involved*
### Page 373 @ 27 May 2023
*'simple' economies are rarely all that simple. They often involve logistical challenges of striking complexity, resolved on a basis of intricate systems of mutual aid, all without any need of centralized control or administration*
### Page 378 @ 28 May 2023
*Times of labour mobilization were thus seen as moments of absolute equality before the gods – when even slaves might be placed on an equal footing to their masters – as well as times when the imaginary city became real, as its inhabitants shed their day-to-day identities*
### Page 378 @ 28 May 2023
*and briefly assembled to become 'the people' of Lagash, or Kish, Eridu, or Larsa as they built or rebuilt some part of the city or the network of irrigation canals that sustained it.*
### Page 378 @ 28 May 2023
*there were other institutions, also said to originate in the Predynastic age, which ensured that ordinary citizens had a significant hand in government. Even the most autocratic rulers of later city-states were answerable to a panoply of town councils, neighbourhood wards and assemblies – in all of which women often participated alongside men.*
### Page 380 @ 28 May 2023
*in the urban societies of neighbouring peoples such as the Hittites, Phoenicians, Philistines and Israelites.52 In fact, it is almost impossible to find a city anywhere in the ancient Near East that did not have some equivalent to a popular assembly*
### Page 380 @ 28 May 2023
*Assyrian emperors like Sennacherib and Ashurbanipal have been famous since biblical times for their brutality, creating monuments that boasted of the bloody vengeance they carried out against rebels. But when dealing with loyal subjects they were strikingly hands-off, often granting near-total autonomy to citizen bodies that made decisions collectively.*
### Page 381 @ 28 May 2023
*Texts found at Nippur give unusual details about the composition of one such assembly, summoned to act as a jury for a homicide case. Among those sitting we find one bird catcher, one potter, two gardeners and a soldier in the service of a temple.*
### Page 381 @ 28 May 2023
*being a manual labourer did not exclude one from direct participation in law and politics*
### Page 383 @ 28 May 2023
*far from needing rulers to manage urban life, it seems most Mesopotamian urbanites were organized into autonomous self-governing units, which might react to offensive overlords either by driving them out or by abandoning the city entirely.*
### Page 384 @ 28 May 2023
*Cuneiform script may well have been invented at Uruk, around 3300 BC, and we can see its early stages of development in numerical tablets and other forms of administrative notation*
### Page 385 @ 28 May 2023
*This sort of arrangement – a series of magnificent, open temples accompanied by a congenial space for public meetings – is exactly what one might expect were Uruk to have been governed by a popular assembly;*
### Page 387 @ 28 May 2023
*Still, the mere existence of a college of scribes administering complicated relations between people, animals and things shows us there was much more going on in the large 'houses of the gods' than just ritual gatherings.*
### Page 388 @ 17 June 2023
*Among them we find the first large-scale dairy and wool production; also the manufacture of leavened bread, beer and wine, including facilities for standardized packaging. Some eighty varieties of fish – freshand saltwater – appear in the administrative accounts along with their associated oil and food products, preserved and stored in temple repositories.*
### Page 389 @ 17 June 2023
*From this we can deduce that a primary economic function of this temple sector was to coordinate labour at key times of year, and to provide quality control for processed goods that differed from those made in ordinary households.*
### Page 400 @ 17 June 2023
*'Uruk expansion', as it is called in the archaeological literature, is puzzling. There's no real evidence of violent conquest, no weapons or fortifications, yet at the same time there seems to have been an effort to transform – in effect, to colonize – the lives of nearby peoples, to disseminate the new habits of urban life.*
### Page 400 @ 17 June 2023
*Temples were established, and with them new sorts of clothing, new dairy products, wines and woollens were disseminated to local populations. While these products might not have been entirely novel, what the temples introduced was the principle of standardization: urban temple-factories were literally outputting products in uniform packages, with the houses of the gods guaranteeing purity and quality control.*
### Page 401 @ 17 June 2023
*From 3100 BC, across the hilly country of what's now eastern Turkey, and then in other places on the edge of urban civilization, we see evidence for the rise of a warrior aristocracy, heavily armed with metal spears and swords, living in what appear to be hill forts or small palaces. All traces of bureaucracy disappear. In their place we find not just aristocratic households – reminiscent of Beowulf's mead hall, or indeed the Pacific Northwest Coast in the nineteenth century – but for the first time also tombs of men who, in life, were clearly considered heroic individuals of some sort,*
### Page 404 @ 18 June 2023
*we cannot possibly hope to trace all these various tendencies back into periods for which no written testimony exists. But it is equally clear that, insofar as modern archaeology allows us to identify an ultimate origin for 'heroic societies' of this sort, it is to be found precisely on the spatial and cultural margins of the world's first great urban expansion*
### Page 405 @ 18 June 2023
*Here we will find further evidence that Bronze Age cities – the world's first large-scale, planned human settlements – could emerge in the absence of ruling classes and managerial elites; but those of the Indus valley also present some uniquely puzzling features, which archaeologists have debated for more than a century.*
### Page 406 @ 18 June 2023
*In the wider ambit of Indus civilization, there is only one rival to Mohenjo-daro: the site of Harappa*
### Page 408 @ 18 June 2023
*Some scholars include only the immediately visible areas of the planned Lower Town and the Upper Citadel as part of the city proper, yielding a total area of 100 hectares. Others note scattered evidence for the city's extension over a far greater area, maybe three times this size – we'd have to call them 'Lower, Lower Towns' – long since submerged by floodplain soils: a poignant illustration of that conspiracy between nature and culture which so often makes us forget that shanty dwellers even exist*
### Page 408 @ 18 June 2023
*Let's consider, for a moment, what archaeology tells us about wealth distribution at Mohenjo-daro. Contrary to what we might expect, there is no concentration of material wealth on the Upper Citadel. Quite the opposite, in fact. Metals, gemstones and worked shell – for example – were widely available to households of the Lower Town*
### Page 409 @ 18 June 2023
*Because of its lack of royal sculpture, or indeed other forms of monumental depiction, the Indus valley has been termed a 'faceless civilization'.92 At Mohenjo-daro, it seems, the focus of civic life was not a palace or cenotaph, but a public facility for purifying the body.*