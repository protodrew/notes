---
tags: [culture, history, anthropology]
type: book
alias: Dawn of Everything
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# The Dawn of Everything

## David Graber and David Wengrow

[[library/book/the_dawn_of_everything/highlights|highlights]]

The dawn of everything is a tough book to pin down ranging in topics from sociology, political philosophy, history, and evolutionary psychology. It seeks to upend the currend understanding of history as a linear progression wherein all disciplines were continuously elevated, as well as the mythic origins of humanity.

### The Origin of "The Origin of Inequality"

The enlightenment was a period where Europeans experienced rapid cultural and intellectual growth thanks to their more frequent interactions with other cultures. While a lot of philosophical texts of the day were framed as a dialogue, many can trace their origins directly to debates about lives importance with people they were meeting for the first time.A major contributing factor to the concept of inequality developing at all was through Jesuit Missionaries and Colonizers attempting to bring their rigid hierarchies to populations of Native Americans.

These ideas were often rejected, not out of some xenophobia, but on intellectual grounds. Many American thinkers asserted that the decision to organize society around ensuring no human can have total authority over another was not an indicator of a primitive lifestyle, but an active decision that is made and reinforced every day. Many Americans at the time observed that the endless power squabbles over the Europeans amounted to a society where everyone was prepared to betray their friends and community members at a moments notice.

Another misonception is the belief that these egalitarian societies couldn't scale. Hunter-gatherer or ([[library/book/the_dawn_of_everything/highlights#Page 147 @ 06 April 2023 03 41 12 PM|immediate return]] systems) were believed to be only small bands who organized on familial bonds, while large groups (socio-psychologist dunbar set the limit at about 150) were seen to necessitate hierarchy and administration. Graeber looks at examples from the America's and Japan

> To construct the earthworks at Poverty Point, for instance, must have taken enormous amounts of human labour and a strict regime of carefully planned-out work, but we still have little idea how that labour was organized. Japanese archaeologists, surveying thousands of years' worth of Jōmon sites, have discovered all sorts of treasures, but they are yet to find indisputable evidence that those treasures were monopolized by any sort of aristocracy or ruling elite
[[library/book/the_dawn_of_everything/highlights### Page 175 @ 16 April 2023 10:38:25 AM|p175]]

### Refusal of Agriculture

There is a common narrative within evolutionary theory that progression is linear. The invention of agriculture appears to have superseded hunting and gathering in our modern society, so the larger discourse settled on hunting and gathering being a purely inferior method of sustinance. This is something that Graber and Wengrow point to as inaccurate, proven by the existence of societies that

### Seasonality

Many non-agricultural societies (and even some agricultural ones) had dynamic social relations that would change during the seasons. The modes of sustinance would necessarily change due to climates. However, the fact that organizational and social systems would change as well suggests a far more advanced political consciousness that has regressed in our society.

They describe the Nambikwara of South Africa: who spent the dry season in small hunting bands, and converged into villages of hundreds. The chief's role would morph to facilitating the mutual aid between all hunting bands, as well as leading by example and winning over the people who would (if they chose) travel with them in the following dry season. As well as the Inuit people who would live in small patriarchal groups during the warmer months, and converge in the colder moths to form large communal villages where everything was freely shared. (for more examples see the Shan and Kanchine peoples of Burma -now Myanmar-, the tribes of the american plains, and the Kwakiutl people of the Pacific Northwest)
