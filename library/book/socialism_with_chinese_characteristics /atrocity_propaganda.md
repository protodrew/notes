---
type:
tags: 
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Atrocity Propaganda

from [[socialism_with_chinese_characteristics]]

Atrocity propaganda is a tool used by imperialist countries to discredit a growing counter-narrative to western hegemony. It is seen within discourse surrounding China in the form of "cultural suppression" in Tibet and Hong Kong, but has also been seen extensively to form the pretense for imperialist conflicts in the past.

> Examples include the struggles in Northern Ireland, the invasion of Iraq, the promotion of the "white Helmets" in Syria, and more recently in relation to Xinjiang in China.

With culturally different nations, this is especially easy when combined with longstanding bigotry that is held by the imperialist west like the "Slavic Barbarians" and "Oriental Mystics" of Russia and China respectively.
