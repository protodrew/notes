---
tags: [marxism, politics, economics, philosophy]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# The China Model

from [[socialism_with_chinese_characteristics]]

The "China Model" is another word for socialism with Chinese characteristics, but seeks to elaborate on the ideological desires of contemporary China with regards to other nations from a more abstract perspective. Put simply

> The fundamental approach of the "China Model" is that China will lead by example and urge other to develop approaches suitable to their own conditions.
> *Roland Boer*

A large factor in this ideology can be traced back to the struggle sessions between Mao and Stalin after the Jiangxi-Fujian Soviety were removed by popular vote. The desire to not have another nation's revolutionary theory applied wholesale to another developing socialist nation.
