---
tags: [marxism, politics, economics, philosophy]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Socialism with Chinese Characteristics: A Guide for Foreigners

## Roland Boer

---

> Some foreigners say that our ideological reform is brainwashing. As I see it, they are correct in what they say. It is washing brains, that's what it is! This brain of mine was washed to become what it is. After joining the revolution, it was slowly washed, washed for several decades. What I received before was all bourgeois education, and even some feudal education.
> *Mao Zedong*

Socialism with Chinese Characteristics attempts to deobfuscate the Marxist tradition in China, and it's path towards socialism for westerners; who through a combination of propaganda and lack of translated information, tend to have a malformed understanding of China's materialist past, present, and future.

[[swcc_in_dialogue_with_marxism]]

[[historical_nihilism]]

[[observing_china_with_western_eyes]]

[[the_china_model]]

[[differences_in_theory]]

[[chinas_relationship_with_philosphy]]
