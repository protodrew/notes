---
tags: [marxism, politics, economics, philosophy]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# China's Relationship with Philosophy

from [[socialism_with_chinese_characteristics]]

China has spent a great deal of time enumerating the practices and philosophies of both Marxism and the Chinese Characteristics that define their unique approach. Philosophy and the social sciences are central to the understanding of a leader's platform, which is delivered both through the abstract of intellectual work and through concrete political actions.

In recent times, Xi has reaffirmed the goal of the social sciences and philosophy should center Marxism. This may lead western leftists to wonder if that doesn't cause a philosophy to define its own outcomes before it begins, but it is important to remember that the philosophy of an era is always a means towards an end. Seeking to maintain philosophy "for its own sake" leads to jargon-filled "ivory tower" philosophy.

> Xi acknowledges that this had been a problem in some quarters, along with lack of competence, the devolution into jargon and textbook language, the sense that Marxism was out of date and simply "ideological", indeed that China was no longer pursuing Marxism at all.
> *Roland Boer*

The leaders exist to steer the discussion by providing a central voice around which the immediate state of affairs can be organized. However, their political writing differs from that of western politicians because it does not exist in a vacuum, and is a synthesis between all the congress' on a subject, as well as the understanding of the thinkers close to the president. It does not exist to retroactively justify the actions of a leader, but to affirm the intentions plainly, so that their actions may be judged against their own words.
