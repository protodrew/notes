---
tags: [productivity, psychology, culture]
type: talk
creation-date: 2023-10-17
modification-date: 2023-11-14
---
# Burnout
## Emily and Amelia Nagoski
### [source](https://www.youtube.com/watch?v=BOaCn9nptN8)

[[talks]]

---

Burnout was a talk given by the Nagoski sisters about what causes and how to combat burnout. The central ideas were focused on burnout being caused by your mind existing in a heightened state for long periods of time, because the threats we face today are far more ephemeral than our ancestors.

To combat this, they offer a few things. Going outside and exercising, which while trite and talked about a lot, really does work wonders. Spending time with loved ones, especially a long hug or kiss, allows the body to relax and gives closure to more abstract struggles.