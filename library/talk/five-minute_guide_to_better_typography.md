---
tags: [web, technology, design]
type: talk
creation-date: 2023-10-17
modification-date: 2023-11-14
---
# A Five-Minute Guide to Better Typography
## Pierrick Calvez
### [Source](https://www.pierrickcalvez.com/journal/a-five-minutes-guide-to-better-typography)

[[talks]]

---

*note* this is taken entirely from the slides found at the source, I didn't attend the talk, but still found some useful information worth keeping

```ad-quote
Type is a beautiful group of letters, not a group of beautiful letters
```

#### Core Ideas

- Think in blocks and hierarchies of elements, use consistent rules for individual blocks, rather than trying to layout the typography of the page as a whole
- Typography is not a science, do things to your eye
- Start with a single typeface that has at least four weights
- use contrast
- skip a weight to deliniate different types of text (headers, subheaders, and paragraphs)
- in doubt, for each type of text, double the size (ex, if paragraph is 25pt, make subheading 50pt and heading 100pt)
- strike a good balance with spacing (Calves advises spacing should be 1.2x-1.5x the font size)
- use between 40 and 70 characters per line
- align left
- don't let font go below 14px