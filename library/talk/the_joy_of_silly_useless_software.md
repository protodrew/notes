---
tags: [art, game_design, design, software]
type: talk
creation-date: 2023-10-17
modification-date: 2023-11-14
---
# The Joy of Silly Useless Software
## Natalie Lawhead
### [source](http://www.nathalielawhead.com/candybox/the-joy-of-silly-useless-software)
---

This is a posted version of a talk that Natalie gave for the [Gamez & ruleZ](https://www.gamezandrulez.ch/) conference.

Natalie starts by talking about how the idea of useless software immediately conjures images of the "old web". Things like Clippy or desktop pets are a retro idea that existed purely as a novelty for a generation growing alongside computers. However, in the current era we have projects like [vscode-pets](https://archive.ph/FcD1W) flip that idea on it's head.

Even in something designed to be as stark and utilitarian as a coding text editor, we still have people devoting time to making little toys that we can use to customize our environments. These design philosophies are not a fad, and the desire to customize and/or inject novelty into an otherwise banal environment is something that will inevitably spring out of a customizable system.

The era of custom MySpace pages and virtual pets may have seemed to come to an end, but that is simply because of their rejection by mainstream software. Modern software relies on a level of brand perception driven in the current era by minimalism and "sleek" interfaces. The opportunity to customize your interaction of a platform could introduce an iota of friction into the experience, or make the product look unappealing to someone glancing over their shoulder. So it had to go.

However, this mindset is still alive and well among indie software and game developers, and can be found across the spectrum of application categories. One that stuck out to we was a piece of software called [StimuWrite](https://eveharms.itch.io/stimuwrite). Which exists as something not designed for the masses, but that serves an amazing purpose to so many people. The questions posed from this are something that I love, but I worry are lost on a lot of people

> Is uniformity progress
> Who are we alienating

Sometimes making something goofy can be user-friendly. Diverging from commonly understood concepts of approachability allows for an opportunity to reach people who are left behind by the current status quo.