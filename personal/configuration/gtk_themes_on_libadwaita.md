---
type: note
tags:
  - linux
  - configuration
  - technology
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Gtk Themes w/ Libadwaita

it can be kind of a pain in the ass changing themes on gnome and libadwaita apps not changing.

globally you can fix this with this line

```bash
echo 'GTK_THEME="THEME_NAME"' | sudo tee -a /etc/environment
```

Then use ctrl-d to end the process. If you want to replace your file entirely just use one right carrot behind cat instead of 2