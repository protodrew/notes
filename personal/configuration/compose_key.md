---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Compose Key

a unique, mappable key that allows the use of combining symbols to create a nonstandard character

a table of all the default bindings can be found [here](https://www.x.org/releases/X11R7.7/doc/libX11/i18n/compose/en_US.UTF-8.html)

If you copy the default binding file, which on Pop!\_OS is at `/usr/share/X11/locale/en_US.UTF-8/Compose`, to `~/.Xcompose`, you can then add your own custom bindings, I have the following addition, with more probably on the way

```sh
<Multi_key> <asterisk> <asterisk>        :"★" U2605 # BLACK STAR
```
