---
type:
tags: 
creation-date: 2023-07-06
modification-date: 2023-11-14
---
# [[how_to_quickly|How to Quickly]] Remove times from Kobo Highlights

I like to use vscode for quick search and replace, the regex for time formatted as HH:MM:SS \[AM/PM] is

```regex
\b\d{1,2}:\d{2}:\d{2} [AP]M\b
```

and replace it with `\n` so that highlights dont clash with the title