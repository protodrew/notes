---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# [[how_to_quickly|How to Quickly]] Restore a Btrfs Snapshot from a Rescue System
## With OpenSUSE

I've made changes to my system on one or more occasions that has messed everything up, if you are in GRUB you can boot into a read only snapshot and then run `snapper rollback`, otherwise u can list snapshots with `snapper list` and rollback with the number as an argument