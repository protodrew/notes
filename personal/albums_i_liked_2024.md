---
type: note
tags:
  - music
  - "#personal"
creation-date: 2024-01-06
modification-date: 2024-01-06
---
# Albums/EPs/Mixtapes/Compilations That I Liked 2023

| Name | Artist | Release Year | Genre |
| ---- | ---- | ---- | ---- |
| RAW PRACTICES VOL. I (​ح​س​ا​س​)  | modest by default | 2023 | vaporwave |
