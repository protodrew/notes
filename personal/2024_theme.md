---
type: note
tags:
  - theme
  - mental_health
creation-date: 2023-12-26
modification-date: 2023-12-26
aliases:
  - embracing the magic
---
# Embracing the Magic

Trying to kill the mundane and the alienating, while growing my ability to communicate beauty.

## [Relevant are.na channel](https://are.na/protodrew/embracing-the-magic)