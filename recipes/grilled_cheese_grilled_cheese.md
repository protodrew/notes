---
type: recipe
tags:
  - recipe
serves: "1"
course: main
description: This is like one of those one pan breakfast sandwiches, but for a grilled cheese, wrote this right after eating it because this needs to be my legacy
creation-date: 2023-10-23
modification-date: 2023-10-23
---
# Grilled Cheese Grilled Cheese

## Ingredients
- kosher (or flaky) salt to taste
- garlic powder to taste
- 2 tbsp fresh basil
- red pepper flakes to taste
- pepper to taste
- 2 slices sourdough bread
- 2 tsp olive oil
- 4 oz low moisture whole milk mozzarella

## Cookware
- medium nonstick skillet

## Steps
 1. slice bread thick, and preheat medium nonstick skillet to medium-high
[sourdough bread: 2 slices]
 2. slice low moisture whole milk mozzarella thin and place on the skillet in an even layer
[low moisture whole milk mozzarella: 4 oz]
 3. sprinkle with garlic powder, sliced fresh basil, red pepper flakes, kosher salt, and pepper
[fresh basil: 2 tbsp; garlic powder; pepper; red pepper flakes; salt]
 4. when cheese becomes golden brown, brush top of bread with olive oil, and flip
[olive oil: 2 tsp]
 5. fold cheese into bread and fold into a sandwich
 6. toast bread on both sides and serve