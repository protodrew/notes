# Spleeter

spleeter is a great commandline tool for splitting a song into it's vocals and accompaniment. install with `pipx install spleeter` and try it with 

```
spleeter separate -p spleeter:2stems -o output input_song.mp3
```

spleeter will create the files `accompaniment.wav` and `vocals.wav` in the folder structure `output_dir/input_song/vocals.wav`