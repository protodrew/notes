---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# brain_dump
from [[godot_wild_jam]]

- a fishing game where you catch different fish based on the phases of the moon
- a surfing game where you change the phases of the moon to influence the tides
- a score based alien shooter that takes place on the moon
- a short narrative game about looking at the moon with a friend
- **surreal mspaint / flying Luna clipper style 3D space alien dating sim (bitcrushed and processed voices) (from [[game_ideas]])**
- **space croquet (suggestion from mom)**
- **juicy as fuck moon pinball game**

ideas expanded on in [[fleshing_out]]