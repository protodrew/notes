---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
from [[godot_wild_jam]]

# Dating Sim

idea, a speed dating night on a diner in the moon, The aliens are all varied and I want the style to be remeniscent of The Flying Luna Clipper. The scope of the game is going to be small, I want it to only remain in the space diner, and there should be about 4 different characters.

Pros

- the art style is something I like a lot and I can easily plunder public domain assets for it
- the writing would be fun and allow me to have some good jokes in there
- I've never made a game like this in godot
- I always love making surreal games

Cons

- I don't want it to just be a visual novel, but in godot. It seems like fun, but if it is a visual novel, I want there to be some interactivity during each date. My ideas for that are
	- each date is a unique minigame where you have to win over their affection
	- each date has a minute to win it style icebreaker
	- each character has a favorite activity you can choose to do with them

# Space Croquet

Idea, you are an earth businessman going to the moon to meet with some aliens you hope to make a deal with on the moon. While there, you play a few rounds of moon croquet and talk shop with them

Pros

- unique concept
- moon's unique gravity and terrain could be fun to mess around with
- would be fun to make a pseudo 3d game in godot's 2d

Cons

- needs a lot of design to figure out the perspective
- doing it in 3d would be something I've never done before, doing it in 2d would present challenges to not make the game look flat
- need to make AI to play against (though I could always just hard code it if it came to that)

# Moon Pinball

Idea, a campy moon pinball board based on the space pinball from the early windows versions. Things like asteroids and aliens could modify the board after a certain score threshold is met

Pros

- fun to make
- fun to play
- simple concept, which leaves ample time for juice

Cons

- not the most original idea