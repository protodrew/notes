---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# art_list

| Name            | Status   | Category       |
| --------------- | -------- | -------------- |
| Angio Neutral   | Complete | Base Character |
| Angio Happy     | Complete | Base Character |
| Angio Sad       | Complete | Base Character |
| Scunge          | Complete | Base Character |
| SQL Neutral     | Complete | Base Character |
| SQL Happy       | Complete | Base Character |
| SQL Sad         | Complete | Base Character |
| Bar Exterior    | Complete | Base BKG       |
| Bar Interior    | Complete | Base BKG       |
| Angio Minigame  | Complete | Minigame BKG   |
| Angio Cards     | Complete | Minigame       |
| SQL Targets     | Complete | Minigame       |
| SQL Minigame    | Complete | Minigame BKG   |
| Scunge Minigame |          | Minigame BKG   |
| Scunge Assets   | Complete | Minigame       |
| Font            | Complete | Base UI        |
| Chatbox         | Complete | Base UI        |
| Menu BKG        | Complete | Base UI        |
| Choice Buttons  | Complete | Base UI        |
|                 |          |                |
