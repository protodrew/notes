---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# A Pirate's Guide to Audio-books

​ In the last year I think I've said to myself "I want to read this, but I don't have time… Let me get it as an audio-book" 100 times.<!--more--> But then I don't because I don't want to support Amazon/Audible, and I can't find a good way to listen to them and find them. So yesterday I sat down and figured out the best way to do it so that I can listen to <u>Debt: The First 5000 Years</u> by David Graeber.

> If you want to see what I've been reading check out my [bookwyrm](https://bookwyrm.social/user/protodrew)

## Finding Your Audio-books

​ If you are anything like me then you live and die by [zlib](https://b-ok.cc) as your source for free E books and Articles, but were struggling to find a good source for audio-books. That was until I found [audiobookbay.net](https://audiobookbay.net), its pretty much the same infrastructure as Zlib, a decentralized set of portals to their vast database of audio-books, with bit-torrent and direct download options. If you are looking for public domain books (a lot of political theory, older classics, and others), then check out [librivox](https://librivox.org/).

    *Alternately, if you can't find it there, you can try and get an invite for [MAM](https://www.myanonamouse.net) or the more exclusive [BiB](https://bibliotik.me/). Though most good Ebook torrent trackers require invite applications and apparently the staff can be pretty harsh on BiB.

​ Audiobookbay's search function can be a bit lacking, so try and include the title without optional punctuation, and the name of the author for best results. You might have to play around with the search field a bit, but I have yet to find a book that wasn't there.

## Listening to Your Audio-books

​ So great, you've got a huge collection of MP3s, time to crack open your phone's default music app and give them a listen right? Well… not quite. That may work for you, but if you don't want to have to remember the time you paused at every time you stop listening, you might want to have a better tool for the job. I personally hate apps that only work for one type of media, so I was less than excited to get some sort of audiobook player. That was until I realized I already had an app on my phone that did the job perfectly, and I'm willing to bet you do too.

​ I listen to a lot of podcasts, and I hate Spotify's podcast infrastructure, so I've always used a separate pod-catcher to do the job. My most recent one is an android app called [Antennapod](https://antennapod.org/). It's free, open source, supports syncing with [gpodder](https://gpodder.github.io/), and has some nice quality of life features like vocal boosting, and truncating silence. However, the feature we are most interested in is it's ability to add a folder from your phone to the list of subscribed podcasts.

​ Once I realized this would make for the perfect audiobook reader, I went ahead and made a folder called audiobooks, slapped the librivox logo in as "cover.png", and then moved my audiobooks into there. Now it saves my progress perfectly, and allows for all those neat features I mentioned earlier. However, its important to keep in mind that that gpodder syncing doesn't seem to work (nor did I expect it to), since this isn't a real podcast feed, so if you absolutely need syncing to other devices you are going to have to look for other options.

    *If you are on IOS, you have a few options. If you want to use a podcast app and don't mind having a 10gb limit, [overcast](https://overcast.fm) will let you do this if you pay for it (I have used overcast for podcasts back when I had an Iphone and loved it). Alternately, I have heard recommendations for **Bookplayer** and **MP3AudiobookPlayer**.

    I hope with these tips you are able to start listening to some nice audiobooks and get through that backlog of books you keep telling yourself you are going to read when you get the chance. Be sure to hit me up on the fediverse [@protodrew@merveilles.town](https://merveilles.town/@protodrew), let me know if this was helpful and what you ended up listening to!

*(information provided by @[contrapak@merveilles.town](mailto:contrapak@merveilles.town))