---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# A Small Update

​	I just wanted to give a little update on the status of this blog, how I'm doing, and what's coming up from me. If you follow me on [mastodon](https://merveilles.town/@protodrew) then you know I've been working on an experimental web game with a friend for one of my college classes. It has been a very demanding project and I faced some challenged throughout, but I am going to be committing much more time to it in the coming weeks up until my class ends. When I get the opportunity to, I usually do a short stream over on my [twitch](https://twitch.tv/protodrew) at least once every few days.

​	I've also been taking time to focus on my mental health and refine what I actually enjoy doing and what projects I should leave behind. The main one for me was server administration. I kept trying to set up various servers to host websites and self hosted projects but doing it on my own hardware in my apartment proved to be a frustrating and demoralizing experience. I think in the future I'll get a vps of some sort to host some projects I want to put up, but I've learned that server admin, hardware, and networking just isn't for me.

​	That being said, this blog is something I want to do more often, I recently was having hesitations about making posts because I thought I didn't have anything to say. I've been trying to interrogate this feeling, and I think it comes down to perfectionism and wanting all of my projects to reflect a unified cohesive brand. I'm done with that. No offense to the people reading this, but I've decided this blog is for me, not for you. If I have something I want to talk about I'm going to put it here, even if someone else explained it better somewhere else. I enjoy writing and want to continue doing it, not just coming up with and throwing away article ideas endlessly. That starts with my next article, which is out right now [A Pirate's Guide to Audiobooks].

​	I want to thank everyone on merveilles for their incredible community and really encouraging me to stop stressing about the things I want to get done, and start doing them. Hopefully you will see much more from me in the near future.