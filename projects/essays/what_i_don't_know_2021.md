---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# What I Don't Know
## 2021

This is going to be my personal recap of the year, as well as me trying a new method of goal setting for the new year. I find resolutions to be wholly ineffective, and I tried [CGP Grey's Theme System](https://www.youtube.com/watch?v=NVGuFdX5guE), but had a hard time giving it any bearing on my day-to-day life. Now that I've picked up journaling again, I'm a bit more cognizant of day-by-day goals to maintain and improve my mental and physical well-being. So I figured I would focus my goals on learning as much information as possible and incorporating my knowledge and goals into my daily routines.

What this means in the concrete is I am going to read more, take more notes on aspects of my life, and overall try and recontextualize my down-time as a time to better myself, rather than turning my brain off and spending time on a video game I've played 1000 times. This isn't to say I'm going to try and transform myself into a "productivity machine", because that's a waste of life and only benefits those who seek to exploit my labor. Rather, I am going to seek to incorporate learning and personal projects into my recreational time, rather than thinking of them as something separate (which leads to them never getting done).

So that brings us to this article. I'm going to go over everything I learned this year that had a big impact on my life, and will go over some of the many many things I don't know, but would like to learn more about, so I can come back to this article if I'm ever feeling project-block, or don't know what to work on next. There's always more to learn and there is so much I want to explore, so obviously this isn't comprehensive, but rather some things I frequently find myself thinking (I need to research this more).

## What I Learned

---

### Godot

I have been working on and off with Godot for a long time, but this was the year I really sat down and learned its workflow inside-out. Obviously game engines have an infinite skill ceiling, but I understand the node-scene paradigm, and am comfortable enough using plugins, signals, and GDscript itself, that there are few roadblocks between my idea and implementing it. I still have a ways to go with 3D, but It's something I am already vastly more comfortable with by the day.

At this point, it is far and away my favorite engine. It is buttery smooth to work with and while it is not without its quirks, every problem I have with it will be addressed in the ever elusive [4.0 release](https://godotengine.org/news). GDscript is a useful abstraction for people with less programming knowledge, or who just don't want to spend the majority of their time futzing around with programming, but you can also connect other languages if you have the desire to, and get the associated benefits of those languages.

### C

I've used C and C# before, but a lot of my classes really went in depth with it in a way that helped me both understand and appreciate the language. I'm novice enough that I still get annoyed with the way they handle strings, and I only half understand how memory management works, but I can now reliably program pretty good in it, and it has joined Java as a language that I can reliably execute pretty complex ideas in.

I'm probably going to expand my knowledge on C going into the new year, and maybe get to the point where I can integrate it with my gamedev either as a language for Godot, or with a framework like [raylib](https://www.raylib.com/).

## Obsidian

Last year was the year I really started taking notes seriously. I listened to the Atomic Notes episode of CGP Grey's podcast Cortex, where he discussed how note-taking never made sense to him in school until he started using the [Zettelkasten](https://en.wikipedia.org/wiki/Zettelkasten) method and found [Obsidian](https://obsidian.md). A lot of my friends maintain personal wikis, and I found the idea pretty solid, and I created an obsidian vault that contains [everything I am learning](https://codeberg.org/protodrew/notes). Its still a process as I develop my note-taking abilities and reduce the friction between my thoughts and a note as I'm on the go, but the software itself is incredible and has made the experience of linking together my thoughts really incredibly simple.

I've always struggled with linear notes, and abandon notebooks constantly if I'm not forced to keep it (and even then I will never look back at them), so the bite sized conceptual notes of obsidian makes the system far more accessible to my scatterbrain.

### CSS

I've tinkered around with webdev for a while, but I never really used CSS to its full potential or really understood what I was copying from [w3schools](https://www.w3schools.com/). So I sat down with the CSS documentation and started to actually understand some of the arcane idiosyncrasies that the language has to offer.

Obviously the labyrinth of CSS knowledge runs deeper than any one person can comprehend, but I now understand the differences between the position indicators, animations, the differences between different browser rendering systems, and a more comprehensive understanding of responsive design.

### What I Don't Know

---

### Blender

All of my roommates are really skilled at 3D modelling, but it's mostly with Maya, Zbrush, and the other associated AAA industry standards. I don't really have a desire to animate and model at that scale, but watching YouTubers like [Miziziziz](https://www.youtube.com/c/Miziziziz), [Garbaj](https://www.youtube.com/c/Garbaj) and communities like [the haunted PS1](https://haunted-ps1.fandom.com/wiki/The_Haunted_PS1_Wiki) have really endeared me to the lofi 3D aesthetic. It's something I want to develop my skills on enough to the point that I could reliably crank out models for my own projects.

I'm choosing to use blender because I'm very passionate about open source tools and want to use it rather than some subscription service that is controlled only in the interests of its shareholders. Blender also has a ton of tools for creative coding that really entice me.

### Writing

I've been writing nonfiction for a long time. I worked as a freelance game journalist from when I was 10 to when I was 17, I've written essays on this blog for a while, and scripts for videos that both have and haven't seen the light of day. However, I've written probably 2 pieces of fiction in the last 5 years, both of which I'm extremely embarrassed by. One of my classes is going to be on writing character for video games, and I'm going to use that as an opportunity to engage with more and write more fiction. Most of it will probably never see the light of day, but I hope its a skill I can build over time to communicate the ideas I want to convey in my games with more subtlety.

### Design History

Aesthetic design in fashion, graphics, and architecture has been a passion of mine for a long time, but it's only recently that I've started actually taking a critical eye to it. Making an [are.na](https://www.are.na/) account led to me discovering the [Consumer Aesthetics Research Institute](https://cari.institute/) and their associated community on [discord](https://discord.gg/cari). I've lurked for a while, but am ready to start actually reading papers from the design firms, understanding the methodology that goes behind crafting aesthetics, and reading journals that discuss similar things in more depth. This is probably the vaguest thing I want to expand my skills on, but with the awesome communities I am a part of, I'm sure it will be a fun and enriching experience.

### 3d Game Design

This ties into blender and learning Godot in 3D more, but I basically want to be able to have a couple short form experimental 3D projects under my belt by the end of 2022, and have a better understanding of things like light maps, shaders, and 3D physics.

### Linguistics

I've been getting into conlang stuff and world building ever since I started watching [jan misali](https://www.youtube.com/channel/UCJOh5FKisc0hUlEeWFBlD-w) and [artifexian](https://www.youtube.com/c/Artifexian), and have found conlanging to be a fascinating vessel for world building. I also have made several attempts to get deeper into my knowledge of Spanish and maybe learn another language, to 0 avail. I think I might be going about it the wrong way, and the duolingo/language class method of learning vocabulary and then memorizing basic grammar rules isn't going to be what helps me.

With that in mind, I'm thinking about spending at least some time this year to learning the conceptual history of linguistics and learning how languages developed in the world and diverged into the languages we now know. Hopefully with this knowledge I can set a goal to learn a language next year, but I'm getting ahead of myself

### Geography

Worldbuilding is something I've always been a fan of, but never really knew how to approach it from any kind of scale. I think this year I'm going to take a step back and learn more about the world itself and how landmasses interact, what the cultural implications of different areas could mean for the structures, both physical and social, of the world.

---

So there it is, a variety of topics that I have learned in 2021 and want to learn in 2022. Hopefully this becomes something of a regular feature on the blog, and I will be back in a year to talk about how I progressed in these topics, and all the other things I come across.

[Follow my progress on mastodon](https://merveilles.town/@protodrew)
