---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# This Tweeting Stuff Will Bum You Out

After around 4 years of hiatus, I set up a crossposter from my mastodon to [@proto_drew](https://twitter.com/proto_drew) on Twitter. I haven't used Twitter since my old account with around 6k followers got suspended out of the blue. I have been exclusive to mastodon and have always told people how much better it makes me feel, and how much more meaningful the interactions have been. However, a few weeks ago I was attending an event for advising game development students at my college, and a lot of the professors said that Twitter remains the best place for developers to network, and if you are using anything it should be that. So I bit down and set up a new one.

It has been exactly 4 weeks since I made that decision, and I have to say it was not for the best. I will continue to operate the crossposter for people who want to follow me there, but I won't be checking it anymore. I want to try and explain why I have had such a bad time with it, and why I think it fails as a platform to produce any meaningful interaction.

## God Damn Quote Retweets

Quote retweets are by far the most annoying part of twitter, it is a system that offers next to no benefits, and produces so much noise that it makes the entire ecosystem worse. However, since I was gone, Twitter decided to make them even more prominently displayed, and added a counter next to retweets, so you can view all of the quote retweets for any one tweet.

Quote retweets aren't a flawed concept inherently, but combined with a lot of factors that built Twitter's culture, they lead to a very negative form of interaction. Every single quote retweet I have seen since I got back on Twitter has been someone dunking on someone else's bad take. This means I get the prize of seeing someone's terrible opinion, and if I'm [doomscrolling](https://en.wikipedia.org/wiki/Doomscrolling), an opportunity to see all the terrible replies that op's followers have made.

I think this has a large psycological effect on me, and I wouldn't be surprised if a lot of other people have similar effects. Seeing not only someone's (likely) racist, transphobic, or otherwise bigoted remark, then having the UI subtly direct you toward actually clicking that remark, and seeing the hundreds of people agreeing with it, presents a very grim view of the online population.

Twitter obviously knows this, but as a platform that lives and dies by user engagement, rather than any form of meaningful interaction, has no incentive or desire to remedy this, and will more than likely continue pushing Quote Retweets as a feature.

## Likes, Retweets, Replies, and "The Algorithm"

On Twitter, there are 3 forms of interaction: Likes, Retweets, and Replies. Likes simply increase the like counter, and have no other form of interaction with the tweet on a system level, retweets allow you to show this to your followers, and replies allow you to reply to something, which is shown in the replies of the tweet, and to your followers. At least, that's what used to be the case around 8 years ago.

Now, likes will occasionally show up in the feed alongside retweets, so the functions have become essentially the same, the only difference being that likes are essentially inconsistent retweets.

Then all of these modes of interaction are fed into an algorithm that analyzes the engagement a tweet is getting, to determine what to show people who search/look at trending content, and to determine what order tweets appear in the timeline (unless you disable that feature, and remember to re-disable it every time twitter randomly enables it). This is an incredibly opaque content-delivery mechanism that allows for Twitter to platform extremely unpopular and extremely popular things next to each other and recommend them both to the average user.

More frequent users have come up with cultural ways to express the popularity of something in a network that only allows for platforming, with the concept of [ratio-ing](https://www.reddit.com/r/OutOfTheLoop/comments/77qch4/what_does_it_mean_to_be_ratioed_on_twitter/), but many cursory viewers will not observe this or really know what to make of it.

## Everything is an Opportunity for Gain

All of this algorithmic recommendation, combined with Twitter having one of the largest userbases of any social media, and their deep involvement with advertising, leads to a desire to turn even the basic interaction model of social media into an exchange and commodity.

This is not a new concept by any means, but it bears repeating. This sort of interaction leads to the synthesis of person and company. The average user needs to act more like a brand if they have any hope of succeeding, and the average brand must act like a person, so their advertising will reach as many people as possible.

---

All of this leads to a further breakdown of communication into another vessel for value exchange, every viral tweet must have a request to follow something, contribute to something, or pay someone. The discourse becomes the culture, and the algorithms do nothing but encourage it, and the end result is a sea of misery. As the great philosopher dril once said "Twitter is like a perpetual funeral being held inside of a circus", and I want no part in it.