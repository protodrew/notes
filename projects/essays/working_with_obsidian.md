---
type:
tags: 
creation-date: 2023-07-05
modification-date: 2023-11-14
---
# How I Take Notes with Obsidian

I got interested in note-taking systems in 2020, and started seriously doing it a few months later. Looking back, I can now tell it was when I was seeing my grandma suffering with the early stages of dementia, and that really scared me. That loss of identity is something that I find existentially terrifying, and has been a motivating factor to take notes seriously. Now I enjoy taking notes for the sake of it, and continuously improving at it and seeing the growth of my vault of knowledge is both satisfying and helpful when I'm learning new skills.

I never really learned how to take notes in school, and didn't get good at it until after I dropped out. I am getting to a system that I find really useful and easy to work with. I've had about 3 distinct "eras" of my obsidian notes, and each time I have learned something more about the way that I want to organize my thoughts and notes. There's no perfect way to take notes, and more than anything I recommend just doing it, and you will figure out over time what works and what doesn't.

I have to preface this by saying that obsidian has changed considerably over the past couple years. When I started using it there was no monetization system other than the option to just give them $20, now they offer both syncing between devices and publishing your vault for monthly fees. I don't use those (I use syncthing² for syncing and will get into publishing a bit later), so I can't speak to that. If any of the more abstract non text-file things obsidian does discourage you, then I would suggest finding a different tool like vimwiki that will suit your needs more. For me, obsidian is the easiest way to capture ideas, journal, and organize my thoughts and things I've learned. I was inspired by Winnie Lim's post³ about starting obsidian to document the way I use obsidian in case it's helpful for anyone else.

## Mindset

Taking notes seems like a very straightforward thing, but if you struggle with a desire to have everything perfect and strive towards some imaginary "optimal" note-taking system, you are doomed. You have to be ok with notes being incomplete, outdated, or in progress. I also caution against trying to notate every thought and thing you learn, which is what I fell into when I started using obsidian a lot. You ultimately want your notes to be something that you refer back to, and so your notes should be something that you want to read.

I also got way into the atomic note idea of each note being a single piece of information, but have since gravitated away from that because it makes the notes a bit less readable or something I was regularly going back to. I know a lot of people really like it, but it just wasn't for me. To develop a system you really like, I recommend looking at how you have historically enjoyed writing or taking notes on things (if you have in the past) and figure out how to mimic that, rather than researching note-taking a ton and trying to fit your brain into someone else's system.

## Some Tips

You can link to specific headings within your wikilinks like: \[\[working_with_obsidian#Tips]]

You can resize images similarly (for some reason this doesn't work for videos) \!\[\[image|width]]

Control-p is your friend, it is useful for both interacting with core obsidian functions and also most of the functionality within extensions. If you're like me, and you suck at remembering hotkeys unless you use them all the time, this is a useful substitute.

Don't configure things in anticipation of how you will use them. You will notice friction points as you get more serious taking notes, and then you can grow into systems.

## Extensions

I try to keep my notes themselves as close to raw markdown as possible for portability and ease of conversion to HTML. I've messed around with dataview, admonition, and a few other popular extensions, but none of them really stuck. However, I use quite a few extensions that I think are helpful.

### Daily Notes

This is what I use for my journaling, my templates are linked below if you are curious. I journal on average once every 2-3 days (always aiming for daily), and it has become the single most beneficial thing I have done regarding note-taking. Keeping track of my thoughts, the things I've done, and what I'm grateful for has made me more introspective and in tune with my own emotions. There is an extension called periodic notes if you want more granular control or things like weekly notes, but the recently built in Daily Notes extension works well for me.

### Templater

Templates were also introduced as a core plugin, but I still use the Templater community extension because it allows setting some variables that will be automatically generated when I make a new note from the templates. Once the note is generated, its just markdown, but this is the closest to an extension that is locked into the obsidian ecosystem.

### Folder Notes

This is a nice way to have a little index note to each of your folders if you are interested in organizing your notes that way. For example, when I am taking notes on a book now I always make a folder that is the name of the book with a folder note that is going to be the main thing I go back to. Then I can have notes that contain highlights or more specific thoughts.

### Note Refactor

I've found it easier when taking notes to do it all in one note and then split it after the fact. This is a simple extension that will let you split your notes into multiple notes based on the headings. While I don't use it a lot, it's always nice when I need it.

### Linter

This is only really relevant if you care about consistent formatting, but it is a pretty cool extension that allows you to have consistent properties around your notes. I've been experimenting with adding YAML front matter to my notes to eventually be able to use it for better sorting later, and the option to make sure I have my tags and other things consistent within my vault.

### Advanced Tables

This is the only extension I would say is a must-have if you are working with tables in obsidian. It makes the experience so much better with navigation, creation, and organizing tables. I can't even express how much better it is because I haven't used the vanilla table system in years. Just do yourself a favor and install this, and you'll thank yourself if you make a table ever.

### Checklist

This is a great way to aggregate TODOs or footnotes or reminders throughout your vault and have them aggregated on the sidebar. There is a more advanced task management tool within obsidian if you feel that you are outgrowing the checklist extension, but I never really messed around with it beyond a surface level introduction.

### Smart Typography

This is a really simple extension that will automatically convert things like three periods into ellipses … → … or two greater-than/less-than signs into guillemets << → " It's a really simple way to make your notes look a bit nicer and I really enjoy the simple aesthetic of having special characters in my notes. This overlaps with Linter a bit, but I still think it's nice to have.

### LanguageTool Integration

The name says it all, it's just a better way to check your grammar than the built-in spell check, my advice is to turn auto-check off and check manually through the command prompt.

## Publishing

$8-10/mo to have my notes visible online is more than I can afford. I want to integrate my notes into my website, but don't want it to rely on external services or excessive JavaScript / fancy effects. I found an extension called Webpage HTML Export that allows you to export your whole vault as HTML, and gives you quite a few options for configuration. I'm going to make a more detailed post about this later because I'm working on some shell scripts that will do some post-processing for my notes to make them a bit more organized and customizable, but for now I would recommend playing around with that extension and seeing if it is something you would be interested in.

## Notes on Mobile

I suck at taking notes with obsidian on my phone, so my system is much leaner there I put everything into a note called "scratch", and then process it when I sync it to my computer. There are a variety of extensions for mobile note-taking in the obsidian app, but I cannot speak to them at all. An important thing to keep in mind is that a lot of extensions won't work on mobile, so if you rely heavily on dataview or other heavy extensions, you will not get to access that on mobile.

links
-----
=> https://syncthing.net/ 2: Syncthing (web link)
=> https://winnielim.org/journal/experimenting-with-obsidian/ 3: Winnie Lim - Experimenting With Obsidian (web link)
=> https://codeberg.org/protodrew/protonexus My obsidian vault (web link)