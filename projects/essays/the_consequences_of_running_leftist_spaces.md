---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# The Consequences of Running Leftist Communities

Today, the administrators of kolektiva.social, a Mastodon instance that bills itself as a "free, non-corporate platform for anarchists and anti-colonial content producers looking to circumvent Google, Facebook, and other surveillance capitalists," announced that an unencrypted copy of their database circa May 2023 was seized by the FBI as part of an unrelated raid. The main content affected as part of this is all user account information and all posts, regardless of the post's privacy level. It should be noted that direct messages on Mastodon require a copy to be stored on both your instance's server and the target server, so the availability of these types of data is not unique. However, the failure to adequately handle potentially sensitive user data and to quickly communicate this breach shows me that kolektiva is falling prey to the lack of meaningful concern for its users that plagues most leftist spaces.

Leftist spaces online are a bit of a paradox. In a neoliberal world that is actively hostile to meaningful anti-capitalist action (see: the admin being raided for being involved in a protest), there can be serious consequences when openly discussing and organizing anti-capitalist theory and action. Despite this, users are rarely adequately informed about the consequences of what they post and the security of their data. On Mastodon and similar ActivityPub-based servers, you should NEVER post anything that you wouldn't want to be publicly associated with your identity. The Fediverse is not a place for secure communication, and the lack of awareness about this has led to people being caught repeatedly with their pants down thinking that their private messages are not secure.

To be clear, this is no worse than corporate social media, which is often more than willing to hand over your unencrypted DMs to any government agency that asks for them. However, spaces that advertise themselves as places for activists to gather and work are essentially creating large databases of leftists that can be easily scooped up by the feds if that data isn't secured.

It's easier than ever to create communities online, especially within the Fediverse, but it takes more than a desire to provide community and lip service to user security and privacy. An unencrypted version of the database should never have been stored anywhere, especially on the local machine of someone who has already drawn the ire of the feds. The insult to the injury is that this was not communicated to users as soon as possible, and their vague hand waving at the discussion of lawyers is not enough to explain the delay. There can be real-life repercussions when running online spaces for both the organizers and users, and many leftist oriented spaces are staffed by people who make up for lacking the ability to protect their users with buzzwords.

I recommend anyone who is a member of kolektiva to move to an instance that will draw less heat, and never talk about anything you wouldn't want a cop to see on there. Good luck to the kolektiva admins in their efforts to demand the FBI return and/or destroy this data, I'm sure the feds will comply.

https://kolektiva.social/@admin/110637031574056150 kolektiva admin announcement