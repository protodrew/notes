---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# I Could Have Done that
## *Observations Working at a museum*

I didn't spend a ton of time with my grandfather as a kid, being that he lived in St. Louis and I lived in Texas. However, it was always fun to visit him as a kid and hang out in his studio. He is a very talented abstract artist who converted a large portion of his basement to painting large abstract oil paintings & acrylics based on reference photographs he took. I grew a lot of my love for art from spending time with him, and through spending that time gained a real appreciation for abstract and minimalist art.

This isn't to say I have some pretentious higher understanding of the meaning behind the piece. Because, as you will soon see, I don't think that it is worth it to "get" every piece of art you come in contact to. I just think that art is neat, and you approach different things from different perspectives.

Recently, I was fortunate enough to land a job as a gallery attendant at my local art museum. I have mixed feelings about the gallery experience, but it is a compromise that I am more than happy with in order to expose art to the public in an educational and largely noncommercial manner. My favorite room to work in is the contemporary and abstract gallery, both because I really enjoy all the pieces, but also because I love to hear what other people have to think.

My favorite reactions are always from children. Their uncapped imagination and general lack of knowledge about how a piece "should" be interpreted lead to far more interesting ideas about the pieces. My favorite in recent memory was a kid who saw the minimalist wooden sculpture that sits in the middle of the room. He walked around it 2 or 3 times and then asked for his mom to pick him up, so he could look down into it. After he had thoroughly inspected the piece, he proudly said to his parents, "I think this is what the inside of a pyramid looks like!" to my delight. He then went on to describe how certain parts were probably where they put the sarcophagus or were structural support.

The only negative attitude I see people express is typically "not getting it", or not seeing the merit of the piece because "I could have done that". Most people try to appeal to this idea by describing the hidden complexity. Barnett Newman alters the base chemistry of his paints, Cy Twombly hides a lot of cryptic metaphors and illusions withing the scrawls of his piece, Carl Andre hides the elements that he grew up with in his installations. All of these things are true, but I think it kind of misses the point. You are right, you could have done it… that's the point. Art museums do not exist to laud the technical complexity of the best artists in the world, they exist to show the art that people resonate with; both that exists and has been created now, and that which has spanned generations.

I think this disdain for the avant-garde comes from 3 different reasons. You have artists, who are upset because it appears like less effort has gone into it vs their art, and it feels annoying to have something that technically simple reach so many more eyes. You also have people who see it most often in the context of extremely plain looking paintings being sold for millions of dollars, and as such view it as just something for rich people to pretend to get. Finally, you have people who do not like the lack of approachable or existing meanings to derive from it, and view it as pretentious nonsense.

That third trait is the one that I will focus on, because I think it is the most interesting. It is certainly informed by the opacity of the "high art" market, and the perceived simplicity of the work juxtaposed with the pieces considered so legendary that they were preserved for 100s of years. I certainly understand why people come to this conclusion, the art market serves only those rich enough to speculate on it, and a lot of the language used when talking about art is only in service of those speculators.

That being said, I would like to invite an alternate way of looking at modern art. This way is less guided, more hippy-dippy, and relies on re-examining the process of viewing art in a gallery. It is to interpret each piece on its own terms. This seems obvious at first, until you realize that the entire process of curation invites categorization. Something isn't just a painting, it's an important indicator of the start of impressionism. The art now serves both its own needs, and the historical significance of that movement. Captured into these bubbles, the art often doesn't ask much of you. It invites comparison between similar pieces, but fails to have people view the role of seeing art in a museum as anything other than a historical or intellectual exercise.

I don't blame people for thinking about art museums that way, the word museum is applied to signify its secondary role: preservation. However, I think it is limiting to approach it from the perspective of a "museum". The act of viewing art does not require an explanation, and any context of the piece itself is information that is fun to learn after you have decided you like art. Imagine if you experienced an album or movie by first reading 2 paragraphs of its entry on Wikipedia. Viewing modern art can sometimes fall apart through this lens. It frequently isn't as technically complex or (yet) historically significant enough to have proven it's worth. A piece of avant-garde art stands in front of you asking to view it, and then think about how you feel about it.

It is totally fair to look at art and dislike it, the purpose of this essay is not to say that you must enjoy all avant-garde art. However, I invite you to examine why you dislike it, and what do you think it is trying to communicate to you. The meaning might not always be deep, but that's ok. Art can exist for reasons beyond communicating incredibly complex themes abstractly (though it happens to be very good at that). Some of the common themes I have seen expressed within modern art that tend to be disliked or misconstrued are

- a desire to showcase certain color combinations, patterns, or palettes
- a desire to have the viewer think about the materials used and how the piece was crafted
- a desire to show the viewer how distinctions between things like gloss and matte, saturation and desaturation, and texture can change the way we look at something

I personally like these themes, if I had to pick pieces from each to recommend it would be (in order):

- Coronation of Sesostris - Cy Twombly
- 144 Lead Square - Carl Andre
- Untitled \[Blue, Green, and Brown\] - Mark Rothko

I hope that you go to your local modern art gallery, and you will notice a number of things that it is difficult to communicate via photography. Things like scale, finer details like gloss and texture, form, and arrangement. For example, in 144 lead square, next to the plates would be a sign advertising the price the plates were purchased with as if it was for sale. This could lead you to think about a lot of things, the absurdity of art's being based off of its material components, and yet the simplistic arrangement of a seemingly low class material invites scorn. In this way, I feel the audiences' reaction to it becomes part of a piece, a sort of litmus test to gauge what people value in art, and what they don't.

Modern art offers to communicate deeply complex ideas, but it can also exist to spurn interpretation in favor of aesthetic experience. It is difficult to categorize modern art as so many of its terms interweave (as a fun example, try to define minimalism, Bauhaus, op-art, and deconstructionism without overlapping), and so many of its terms also seek to communicate a vibe or a group, yet are used interchangeably. I invite you to come up with your own category, or ignore categorization all together.

I love when people ask me about the pieces I watch over, because I usually hear what they have said about it, and try to either offer my perspective or illuminate parts of it that give it a better context within a room of so many disparate pieces. It is through this essay that I hope to do this on a much larger scale. If you would ever like to talk about art I will lend my ears/eyes/fingers at @protodrew@merveilles.town, and I hope I can encourage you to view things through a new lens.