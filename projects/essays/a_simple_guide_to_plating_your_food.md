---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# A Simple Guide to Plating Your Food

*for visual examples, see my [pixelfed](https://pixelfed.social/drews-foods)*

I love cooking, for myself and others. Making food is one of my favorite joys, and sharing it with others is so much more than providing sustinance. It is an artform where I can arrive at a finished piece in mere minutes, and share it with a small, intimate audience whose reactions I get to directly experience.

Many others don't view it the same, and it's understandable why. In a world where every minute of your time exists to serve the needs of capital, it is hard to sit down and enjoy simple things because they feel like minutes wasted. In lieu of encouraging making more complex food (because that is only something that an individual can decide for themselves), I will provide 4 simple tips to make plating your food better.

## Why is Plating Important?

If I'm making a plate of grits, instant oatmeal, or ramen, it's easy to want to just slop it on and ignore the look. Many people's home cooked meals are done this way, and it's understandable. Plating is seen as the perview of chefs and those who wish to make a fancy meal. However, plating actually matters quite a bit and will help elevate even simple meals.

Plating is important because eating is a visual medium, everybody likes to bring up the fact that most of the sensation of taste is smell, but nobody takes appearance into account at all. Appearance and plating is the factor that allows fancy restaurants to pass off a much smaller or otherwise less impressive meal, and still charge far more than the pure worth of the food. So why not do it for your own food? If you can make a plate that is really nice, it will help make the meal feel more complete, and the effort of producing it more worthwhile.

## 1 - Verticality

The easiest and most impactful thing to do is give food some verticality. Put the pasta in a little twisted nest, stack a hearty protein on top of some robust veggies, put your fried rice in a greased measuring cup and use that to make a little shape like they do at chinese restaurants.

It's a super simple thing to do, but it does a lot to make the meal pop off the plate and look more varied. If everything is just slopped on the plate, then it looks flat, and like you are eating the same thing over and over again.

## 2 - Form

Form is a bit broad, but it's the best name I could think of for this concept. The basic idea is to consider how your food occupies a plate. Make use of whitespace, and organizing things next to, or around other things.

Let me take the most stripped down example. You just pulled out a tray of nuggets from the oven, and want to have them with some ketchup. Your first instinct is to just put them on the plate, scoot them over, so you can have space for your sauce, and slop it right on the plate.

Instead, consider putting the sauce in a small bowl or ramican, and then put that in the middle of the plate. You may not have changed the meal entirely, but now you have a bit more shape and form added to your meal

## 3 - Color

This is something to keep in mind while you are cooking, and not after, but doesn't require a ton of thought. Keep in mind the colors you are including and how to make it pop, if you're making a nice red curry, don't pour it directly on the rice, put the rice up against the curry so that it can be mixed in on by the person eating the food.

Garnishing is also not a worthless addition to a meal, and can really add a bit of textural and color variety to the meal, having some parsely, cilantro, and white and black sesame seeds on hand can never do you wrong. Also, If you live in an area and can do it easily, grow some edible flowers, it doesn't matter what you add them to, it will always look impressive.

## 4 - Separation

This covers a couple of things, but the main element is using whitespace on the plate to distinguish elements, or the opposite to combine them. My girlfriend recently made us some roasted cauliflower with a balsamic and rasin glaze, and I whipped up some basic tofu triangles, and a balsamic/olive oil mixture to dip them. The balsamic and olive oil mixture went onto a plate on the side to allow for communal dipping, and a nice amount of space on the plate was left to separate the triangles from the vegetables.

This was done both to help avoid the mixing of flavors, but to illustrate that although similar in flavor (writing about flavor should be an atricle of its own), they are not to be eaten together.

Consider the opposite case, if you are making a steak with asparagus, and want to have both enjoyed with a mushroom sauce, stack the steak on a bed of asparagus (remember… verticality), and drape the sauce across both.

So there you have it, my simple tips for plating food, feel free to share photos with me on the fediverse (masto link below, pixelfed link above), and let me know if this article was helpful for you!