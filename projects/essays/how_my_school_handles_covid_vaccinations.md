---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# How My School Handles COVID Vaccinations

I was recently given the opportunity to get a vaccination through my University due to being in some categories deemed as high risk. I received the vaccine 1 week ago on the 25, and will give you the rundown on how my University allowed us to apply, what the process was like, and some general musings on the vaccination process in general.

<!--more-->

## Part 1, Announcement and Application

We received emails that the School (which for privacy I will only let you know is in the northeast of the United States) starting early February that the school was planning on administering the Moderna variant of the vaccine both to the University community and the larger community in the city. We were told the following:

> Phase 1 focused on healthcare providers and first responders, and WPI worked with the commonwealth to secure and administer the vaccine to our campus health and safety staff—we are now working on a plan to access vaccines in a timely manner under Phase 2.

> Phase 2 includes four groups related to age, medical conditions, certain job categories, and other factors. Per the state's guidelines, we are planning to administer vaccinations to the small number of community members eligible in Phase 2, Group 1, which includes only those age 75 and over. We have already communicated directly with the individuals who are eligible.

Everyone was invited to submit a form that allowed us to state any risk factors we may fall under and then we were told we would be notified. I was fortunate enough to meet enough categories to fall under phase 2 group 2. I received an email about 1 week later informing me I was eligible and to schedule a time for the following week.

## Part 2, Arriving and Receiving

On the 25th I arrived to the designated locations with my two required forms (a medical consent form and a pre-vaccination screening form) and my student ID. I went in, my papers were checked briefly, and then I proceeded into the testing zone.

![](https://i.imgur.com/cgx1Nfs.jpg)

*the vaccination room*

After I was done waiting in the short line. I sat down, and was given the vaccine. The entire process took about 5 minutes from entering the room to receiving the shot. I was then given a standard CDC vaccine card with my second dosage being the following month. After all of this concluded I sat in the waiting area for 15 minutes as instructed, and after not feeling any adverse side effects, I was able to leave.

## Part 3, Thoughts

Overall I am very impressed with the logistics that my school has had throughout the entire pandemic, we have been tested 2x a week for COVID-19 and utilize a symptom tracker via a Microsoft Power App. My school also has a public facing COVID Dashboard that shows the number of students currently in quarantine and isolation respectively, as well as the number of COVID tests for the past 7 and 30 days, and the number of tests administered.

With this in mind I expected the vaccination process to be equally organized, but nevertheless was surprised by the expedience and professonalism that the entire process contained. I hope that with this level of organization soon this entire city and university will be able to receive the COVID-19 vaccine, and I will update with what happened upon receiving my second dose.
