---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Reflections on Burnout

I have not been able to work on projects recently.

It's not just that I haven't found something that excited me, but I have been struggling to stay on one idea for more than a day, learning new things is exhausting and frustrating, working on old things feels redundant and annoying, and my imposter syndrome is constantly telling me I'll never be able to make something that I'm proud of.

I've been pushing myself too hard for too long, and a series of academic and professional failures have left me less sure of what I want to do in the future. It's hard to admit that I have burnout at only 20, but looking back, I've been pushing myself to make things on the internet for 7 years straight. I've worked 10-hour days on top of schoolwork, put my heart and soul into projects that generated 0 return, and over time continued to overpromise both towards myself and to others.

It's tough to reconcile the number of completed projects I have, the number of completed projects I want, and the relatively short amount of time I've been actively engaged in learning these things. I constantly feel isolated from my peers who aren't working on the kinds of projects I like, yet feel behind those that are.

This paralysis is the root cause of what was causing me to perform poorly. I've been existentially unsure of my future, sticking to a script I wrote when I wasn't even planning on living to 20. Now things are different, my life is so much better in enumerable ways, yet I still want to create big, amazing, show-stopping projects. For now though…

Outside of projects, I've been advancing my life and trying to become more self-sufficient. I managed to get a high paying job at a near-ish commercial kitchen, which gives me more freedom in recipe interpretations and a calmer work environment than I expected. I've also been spending more time looking inward, meditating, learning new breathing exercises, figuring out what motivates me and what I want my future to look like.

I wish I had a clear answer as to how I'm going to move forward, or what steps I can put in place to heal my burnout. For now, I'm mostly going to try reading books, playing games I enjoy, and learning to derive satisfaction from the other elements of my life. Still going to be active on merveilles and chilling, but will mostly be avoiding talking about personal projects unless it's something I'm REALLY sure will be seen through to completion.

I started this year with the theme of constructing the foundation¹, and have found myself building more from scratch than I originally expected. That being said, I have no life other than my own, and am excited to delve into the future.

\[Links\]

1 https://codeberg.org/protodrew/notes/src/branch/main/personal/2022_theme.md